package com.epam.epamfinaltask.dao;

import java.util.List;

import com.epam.epamfinaltask.domain.Request;
import com.epam.epamfinaltask.exception.DAOException;
/**
 * This interface represents a contract for a DAO for the {@link Request} model.
 *
 * @author Polina
 */
public interface RequestDao extends Dao<Request> {
	/**
     * Returns  a list of all requests from the database matching the given user id, otherwise null.
     * @param role The role of the user to be returned.
     * @return A list of all requests from the database matching the given user id, otherwise null.
     * @throws DAOException If something fails at database level.
     */
	List<Request> searchUsersRequests(int id) throws DAOException;
	/**
     * Returns  a list of all requests from the database matching the given driver id, otherwise null.
     * @param role The role of the user to be returned.
     * @return A list of all requests from the database matching the given driver id, otherwise null.
     * @throws DAOException If something fails at database level.
     */
	List<Request> searchDriversRequests(int id) throws DAOException;
	/**
     * Returns a list of all requests from the database ordered by request ID. The list is never null and
     * is empty when the database does not contain any user.
     * @return A list of all requests from the database ordered by request ID.
     * @throws DAOException If something fails at database level.
     */
	List<Request> requestList() throws DAOException;

}
