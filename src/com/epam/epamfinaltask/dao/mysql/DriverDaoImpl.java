package com.epam.epamfinaltask.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.epam.epamfinaltask.dao.DriverDao;
import com.epam.epamfinaltask.domain.Category;
import com.epam.epamfinaltask.domain.Driver;
import com.epam.epamfinaltask.domain.Passenger;
import com.epam.epamfinaltask.exception.DAOException;

/**
 * This class represents a concrete JDBC implementation of the {@link DriverDAO}
 * interface.
 *
 * @author Polina
 *
 */
public class DriverDaoImpl extends BaseAbstractDao implements DriverDao {
	private static Logger logger = Logger.getLogger(DriverDaoImpl.class);

	private static final String SQL_FIND_BY_ID = "SELECT `isBan`, `reputation`, `isActive`, `isFree`, `login`, `password` FROM `driver` WHERE `id` = ?";
	private static final String SQL_LIST_WITH_ACTIVE_DR = "SELECT  `id`, `isBan`, `reputation`, `isFree`, `login`, `password` FROM `driver` WHERE `isActive` = '1' ";
	private static final String SQL_LIST_ORDER_BY_ID = "SELECT  `isBan`, `reputation`, `isActive`, `isFree`, `login`, `password` FROM `driver` ORDER BY `id`";
	private static final String SQL_LIST_WITH_ACTIVE_FREE_DR = "SELECT `id`, `isBan`, `reputation`, `isFree`, `login`, `password` FROM `driver` WHERE `isActive` = '1' AND `isFree`='1'";
	private static final String SQL_INSERT = "INSERT INTO `driver` (`id`, `isBan`, `reputation`, `isActive`, `isFree`, `login`, `password`) VALUES (?, ?, ?, ?, ?)";
	private static final String SQL_UPDATE = "UPDATE `driver` SET `isBan`= ?, `reputation`= ? ,"
			+ " `isActive`= ?, `isFree` = ?, `login` = ?, `password` = ? WHERE `id` = ?";
	private static final String SQL_FIND_BY_LOGIN_PASSWORD =
			"SELECT `id`, `isBan`, `reputation`, `isActive`,"
			+ " `isFree`, `login`, `password`  FROM `driver` WHERE  `password` = ? AND `login` = ?";

	private static final String SQL_DELETE = "DELETE FROM `driver` WHERE `id` = ?";

	@Override
	public Integer create(Driver user) throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);
			statement.setInt(1, user.getUser().getIdentity());
			statement.setBoolean(2, user.isBan());
			statement.setDouble(3, user.getReputation());
			statement.setBoolean(4, user.getActive());
			statement.setBoolean(5, user.getFree());
			statement.setString(6, user.getLogin());
			statement.setString(7, user.getPassword());
			statement.executeUpdate();
			resultSet = statement.getGeneratedKeys();
			if (resultSet.next()) {
				return resultSet.getInt(1);
			} else {
				logger.error("There is no autoincremented index after trying to add record into table `users`");
				throw new DAOException(
						"There is no autoincremented index after trying to add record into table `users`");
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				resultSet.close();
			} catch (SQLException | NullPointerException e) {
			}
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}

	@Override
	public Driver find(Integer id) throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_FIND_BY_ID);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			Driver user = null;
			if (resultSet.next()) {
				user = new Driver();
				user.setIdentity(id);
				user.setBan(resultSet.getBoolean("isBan"));
				user.setReputation(resultSet.getInt("reputation"));
				user.setBan(resultSet.getBoolean("isActive"));
				user.setBan(resultSet.getBoolean("isFree"));
				user.setLogin(resultSet.getString("login"));
				user.setPassword(resultSet.getString("password"));

			}
			return user;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				resultSet.close();
			} catch (SQLException | NullPointerException e) {
			}
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}

	@Override
	public void update(Driver user) throws DAOException {
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(SQL_UPDATE);

			statement.setInt(1, user.getUser().getIdentity());
			statement.setBoolean(2, user.isBan());
			statement.setDouble(3, user.getReputation());
			statement.setBoolean(4, user.getActive());
			statement.setBoolean(5, user.getFree());
			statement.setString(6, user.getLogin());
			statement.setString(7, user.getPassword());

			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}

	}

	@Override
	public void delete(Integer id) throws DAOException {
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(SQL_DELETE);
			statement.setInt(1, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}

	@Override
	public List<Driver> driversList() throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_LIST_ORDER_BY_ID);
			resultSet = statement.executeQuery();
			List<Driver> users = new ArrayList<>();
			Driver user = null;
			while (resultSet.next()) {
				user = new Driver();
				user.setIdentity(resultSet.getInt("id"));
				user.setBan(resultSet.getBoolean("isBan"));
				user.setReputation(resultSet.getInt("reputation"));
				user.setBan(resultSet.getBoolean("isActive"));
				user.setBan(resultSet.getBoolean("isFree"));
				user.setLogin(resultSet.getString("login"));
				user.setPassword(resultSet.getString("password"));
				users.add(user);
			}
			return users;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				resultSet.close();
			} catch (SQLException | NullPointerException e) {
			}
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}

	@Override
	public List<Driver> searchActiveDriver() throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_LIST_WITH_ACTIVE_DR);
			resultSet = statement.executeQuery();
			List<Driver> users = new ArrayList<>();
			Driver user = null;
			while (resultSet.next()) {
				user = new Driver();
				user.setIdentity(resultSet.getInt("id"));
				user.setBan(resultSet.getBoolean("isBan"));
				user.setReputation(resultSet.getInt("reputation"));
				user.setBan(resultSet.getBoolean("isActive"));
				user.setBan(resultSet.getBoolean("isFree"));
				user.setLogin(resultSet.getString("login"));
				user.setPassword(resultSet.getString("password"));
				users.add(user);
			}
			return users;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				resultSet.close();
			} catch (SQLException | NullPointerException e) {
			}
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}
	
	
	 @Override
	    public Driver searchDriver(String login, String password) throws DAOException {
	        return find(SQL_FIND_BY_LOGIN_PASSWORD, login, password);
	    }

	    /**
	     * Returns the user from the database matching the given SQL query with the given values.
	     * @param sql The SQL query to be executed in the database.
	     * @param login The PreparedStatement value to be set.
	     * @param password PreparedStatement value to be set.
	     * @return The user from the database matching the given SQL query with the given values.
	     * @throws DAOException If something fails at database level.
	     */
	    private Driver find(String sql, String login, String password) throws DAOException {
	    	PreparedStatement statement = null;
			ResultSet resultSet = null;
			try {
				statement = connection.prepareStatement(SQL_FIND_BY_LOGIN_PASSWORD);
				statement.setString(1, login);
				statement.setString(2, password);
				resultSet = statement.executeQuery();
				Driver user = null;
				if (resultSet.next()) {
					user = new Driver();
					user.setIdentity(resultSet.getInt("id"));
					user.setLogin(login);
					user.setPassword(password);
					user.setBan(resultSet.getBoolean("isBan"));
					user.setReputation(resultSet.getInt("reputation"));
					user.setBan(resultSet.getBoolean("isActive"));
					user.setBan(resultSet.getBoolean("isFree"));
					user.setLogin(resultSet.getString(login));
					user.setPassword(resultSet.getString(password));
				}
				return user;
			} catch (SQLException e) {
				throw new DAOException(e);
			} finally {
				try {
					resultSet.close();
				} catch (SQLException | NullPointerException e) {
				}
				try {
					statement.close();
				} catch (SQLException | NullPointerException e) {
				}
			}
	    }
	


	@Override
	public List<Driver> searchFreeActiveDriver() throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_LIST_WITH_ACTIVE_FREE_DR);
			resultSet = statement.executeQuery();
			List<Driver> users = new ArrayList<>();
			Driver user = null;
			while (resultSet.next()) {
				user = new Driver();
				user.setIdentity(resultSet.getInt("id"));
				user.setBan(resultSet.getBoolean("isBan"));
				user.setReputation(resultSet.getInt("reputation"));
				user.setBan(resultSet.getBoolean("isActive"));
				user.setBan(resultSet.getBoolean("isFree"));
				users.add(user);
			}
			return users;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				resultSet.close();
			} catch (SQLException | NullPointerException e) {
			}
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}

}
