package com.epam.epamfinaltask.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;

import com.epam.epamfinaltask.dao.TaxiDao;
import com.epam.epamfinaltask.domain.Taxi;
import com.epam.epamfinaltask.exception.DAOException;
/**
 * This class represents a concrete JDBC implementation of the {@link TaxiDAO}
 * interface.
 *
 * @author Polina
 *
 */
public class TaxiDaoImpl extends BaseAbstractDao implements TaxiDao {

	private static Logger logger = Logger.getLogger(TaxiDaoImpl.class);

	private static final String SQL_FIND_BY_ID = "SELECT `auto_number`, `image`, `is_free` FROM `taxi` WHERE `id` = ?";
	private static final String SQL_LIST_ORDER_BY_ID = "SELECT  `auto_number`, `image`, `is_free` FROM `taxi` ORDER BY `id`";
	private static final String SQL_LIST_WITH_FREE_TAXI = "SELECT `id`, `auto_number`, `image` FROM `taxi` WHERE `is_free`='1'";
	private static final String SQL_INSERT = "INSERT INTO `taxi` (`auto_number`, `image`, `is_free`) VALUES (?, ?, ?)";
	private static final String SQL_UPDATE = "UPDATE `taxi` SET `auto_number`= ?, `image`= ? , `is_free`= ? WHERE `id` = ?";
	private static final String SQL_DELETE = "DELETE FROM `taxi` WHERE `id` = ?";
	private static final String DEFAULT_PATH = "${pageContext.request.contextPath}/img/default_car.jpg";
	@Override
	public Integer create(Taxi taxi) throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, taxi.getAutoNumber());
			Optional<String> str = taxi.getImage();
			String image = null;
			if (str.isPresent()) {
				image = str.get();
			} else { image = DEFAULT_PATH; }
			statement.setString(2, image);
			statement.setBoolean(3, taxi.getFree());
			statement.executeUpdate();
			resultSet = statement.getGeneratedKeys();
			if (resultSet.getInt(4)!= -1) {
				return resultSet.getInt(4);
			} else {
				logger.error("There is no autoincremented index after trying to add record into table `users`");
				throw new DAOException(
						"There is no autoincremented index after trying to add record into table `users`");
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				resultSet.close();
			} catch (SQLException | NullPointerException e) {
			}
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}

	@Override
	public Taxi find(Integer id) throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_FIND_BY_ID);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			Taxi taxi = null;
			if (resultSet.next()) {
				taxi = new Taxi();
				taxi.setIdentity(id);
				taxi.setFree(resultSet.getBoolean("is_free"));
				taxi.setImage(resultSet.getString("image"));
				taxi.setAutoNumber(resultSet.getString("auto_number"));
			}
			return taxi;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				resultSet.close();
			} catch (SQLException | NullPointerException e) {
			}
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}

	@Override
	public void update(Taxi taxi) throws DAOException {
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(SQL_UPDATE);

			statement.setString(1, taxi.getAutoNumber());
			Optional<String> str = taxi.getImage();
			String image = null;
			if (str.isPresent()) {
				image = str.get();
			} else { image = DEFAULT_PATH; }
			statement.setString(2, image);
			statement.setBoolean(3, taxi.getFree());
			statement.setInt(4, taxi.getIdentity());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}

	}

	@Override
	public void delete(Integer id) throws DAOException {
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(SQL_DELETE);
			statement.setInt(1, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}


	@Override
	public List<Taxi> taxiList() throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_LIST_ORDER_BY_ID);
			resultSet = statement.executeQuery();
			List<Taxi> taxis = new ArrayList<>();
			Taxi taxi = null;
			while (resultSet.next()) {
				taxi = new Taxi();
				taxi.setIdentity(resultSet.getInt("id"));
				taxi.setFree(resultSet.getBoolean("is_free"));
				taxi.setImage(resultSet.getString("image"));
				taxi.setAutoNumber(resultSet.getString("auto_number"));
				taxis.add(taxi);
			}
			return taxis;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				resultSet.close();
			} catch (SQLException | NullPointerException e) {
			}
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}

	@Override
	public List<Taxi> searchTaxi() throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_LIST_WITH_FREE_TAXI);
			resultSet = statement.executeQuery();
			List<Taxi> taxis = new ArrayList<>();
			Taxi taxi = null;
			while (resultSet.next()) {
				taxi = new Taxi();
				taxi.setIdentity(resultSet.getInt("id"));
				taxi.setFree(resultSet.getBoolean("is_free"));
				taxi.setImage(resultSet.getString("image"));
				taxi.setAutoNumber(resultSet.getString("auto_number"));
				taxis.add(taxi);
			}
			return taxis;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				resultSet.close();
			} catch (SQLException | NullPointerException e) {
			}
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}

}
