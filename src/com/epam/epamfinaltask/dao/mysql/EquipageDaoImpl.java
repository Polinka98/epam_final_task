package com.epam.epamfinaltask.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.epam.epamfinaltask.dao.EquipageDao;
import com.epam.epamfinaltask.domain.Equipage;
import com.epam.epamfinaltask.exception.DAOException;
/**
 * This class represents a concrete JDBC implementation of the {@link EquipageDAO}
 * interface.
 *
 * @author Polina
 *
 */
public class EquipageDaoImpl extends BaseAbstractDao implements EquipageDao {
	private static Logger logger = Logger.getLogger(EquipageDaoImpl.class);

	private static final String SQL_FIND_BY_ID = "SELECT `is_free`, `driver_id`, `taxi_id`, `current_coord_longitude`, `current_coord_latitude` FROM `driver_taxi` WHERE `id` = ?";
	private static final String SQL_FIND_BY_DRIVERID = "SELECT  `is_free`, `is_free`, `taxi_id`, `current_coord_longitude`, `current_coord_latitude` FROM `user` WHERE `driver_id` = ? ";
	private static final String SQL_LIST_ORDER_BY_ID = "SELECT  `is_free`, `driver_id`, `taxi_id`, `current_coord_longitude`, `current_coord_latitude` FROM `driver_taxi` ORDER BY `id`";
	private static final String SQL_LIST_WITH_FREE_EQUIPAGE = "SELECT `id`,  `driver_id`, `taxi_id`, `current_coord_longitude`, `current_coord_latitude` FROM `driver_taxi` WHERE `is_free` = '1'";
	private static final String SQL_INSERT = "INSERT INTO `driver_taxi` (`is_free`, `driver_id`, `taxi_id`, `current_coord_longitude`, `current_coord_latitude`) VALUES (?, ?, ?, ?, ?)";
	private static final String SQL_UPDATE = "UPDATE `driver_taxi` SET `is_free` = ?, `driver_id` = ?, `taxi_id` = ?, `current_coord_longitude` = ?, `current_coord_latitude` = ? WHERE `id` = ?";
	private static final String SQL_DELETE = "DELETE FROM `driver_taxi` WHERE `id` = ?";
	@Override
	public Integer create(Equipage eq) throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);
			statement.setBoolean(1, eq.getFree());
			statement.setInt(2, eq.getDriverId());
			statement.setInt(3, eq.getTaxiId());
			statement.setDouble(4, eq.getCurrentCoordLond());
			statement.setDouble(5, eq.getCurrentCoordLat());
			
			statement.executeUpdate();
			resultSet = statement.getGeneratedKeys();
			if (resultSet.next()) {
				return resultSet.getInt(1);
			} else {
				logger.error("There is no autoincremented index after trying to add record into table `users`");
				throw new DAOException(
						"There is no autoincremented index after trying to add record into table `users`");
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				resultSet.close();
			} catch (SQLException | NullPointerException e) {
			}
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}

	@Override
	public Equipage find(Integer identity) throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_FIND_BY_ID);
			statement.setInt(1, identity);
			resultSet = statement.executeQuery();
			Equipage eq = null;
			if (resultSet.next()) {
				eq = new Equipage();
				eq.setIdentity(identity);
				eq.setFree(resultSet.getBoolean("is_free"));
				eq.setDriverId(resultSet.getInt("driver_id"));
				eq.setTaxiId(resultSet.getInt("taxi_id"));
				eq.setCurrentCoordLond(resultSet.getDouble("current_coord_longitude"));
				eq.setCurrentCoordLat(resultSet.getDouble("current_coord_latitude"));
			}
			return eq;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				resultSet.close();
			} catch (SQLException | NullPointerException e) {
			}
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}

	@Override
	public void update(Equipage eq) throws DAOException {
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(SQL_UPDATE);
			statement.setInt(1, eq.getIdentity());
			statement.setBoolean(2, eq.getFree());
			statement.setInt(3, eq.getDriverId());
			statement.setInt(4, eq.getTaxiId());
			statement.setDouble(5, eq.getCurrentCoordLond());
			statement.setDouble(6, eq.getCurrentCoordLat());
			
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}

	@Override
	public void delete(Integer identity) throws DAOException {
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(SQL_DELETE);
			statement.setInt(1, identity);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}

	@Override
	public Equipage searchEquipageByDriverId(int driverId) throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_FIND_BY_DRIVERID);
			statement.setInt(1, driverId);
			resultSet = statement.executeQuery();
			Equipage eq = null;
			if (resultSet.next()) {
				eq = new Equipage();
				eq.setIdentity(resultSet.getInt("id"));
				eq.setFree(resultSet.getBoolean("is_free"));
				eq.setDriverId(resultSet.getInt(driverId));
				eq.setTaxiId(resultSet.getInt("taxi_id"));
				eq.setCurrentCoordLond(resultSet.getDouble("current_coord_longitude"));
				eq.setCurrentCoordLat(resultSet.getDouble("current_coord_latitude"));
			}
			return eq;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				resultSet.close();
			} catch (SQLException | NullPointerException e) {
			}
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}

	@Override
	public List<Equipage> equipageList() throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_LIST_ORDER_BY_ID);
			resultSet = statement.executeQuery();
			List<Equipage> equipages = new ArrayList<>();
			Equipage eq = null;
			while (resultSet.next()) {
				eq = new Equipage();
				eq.setIdentity(resultSet.getInt("id"));
				eq.setFree(resultSet.getBoolean("is_free"));
				eq.setDriverId(resultSet.getInt("driver_id"));
				eq.setTaxiId(resultSet.getInt("taxi_id"));
				eq.setCurrentCoordLond(resultSet.getDouble("current_coord_longitude"));
				eq.setCurrentCoordLat(resultSet.getDouble("current_coord_latitude"));
				equipages.add(eq);
			}
			return equipages;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				resultSet.close();
			} catch (SQLException | NullPointerException e) {
			}
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}

	@Override
	public List<Equipage> searchFreeEquipage() throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_LIST_WITH_FREE_EQUIPAGE);
			resultSet = statement.executeQuery();
			List<Equipage> equipages = new ArrayList<>();
			Equipage eq = null;
			while (resultSet.next()) {
				eq = new Equipage();
				eq.setIdentity(resultSet.getInt("id"));
				eq.setFree(resultSet.getBoolean("is_free"));
				eq.setDriverId(resultSet.getInt("driver_id"));
				eq.setTaxiId(resultSet.getInt("taxi_id"));
				eq.setCurrentCoordLond(resultSet.getDouble("current_coord_longitude"));
				eq.setCurrentCoordLat(resultSet.getDouble("current_coord_latitude"));
				equipages.add(eq);
			}
			return equipages;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				resultSet.close();
			} catch (SQLException | NullPointerException e) {
			}
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}

}
