package com.epam.epamfinaltask.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.epam.epamfinaltask.dao.PassengerDao;
import com.epam.epamfinaltask.domain.Category;
import com.epam.epamfinaltask.domain.Passenger;
import com.epam.epamfinaltask.domain.Role;
import com.epam.epamfinaltask.domain.User;
import com.epam.epamfinaltask.exception.DAOException;

public class PassengerDaoImpl extends BaseAbstractDao implements PassengerDao {

	private static Logger logger = Logger.getLogger(PassengerDaoImpl.class);

	private static final String SQL_FIND_BY_ID =
			"SELECT `cash_account`, `status`, `isBan`, `reputation`, `login`, `password` FROM `passenger` WHERE `id` = ?";
	private static final String SQL_LIST_ORDER_BY_ID =
			"SELECT  `cash_account`, `status`, `isBan`, `reputation`, `login`, `password` FROM `passenger` ORDER BY `id`";
	private static final String SQL_INSERT =
			"INSERT INTO `passenger` (`id`, `cash_account`, `status`, `isBan`, `reputation`, `login`, `password`) VALUES (?, ?, ?, ?, ?, ?, ?)";
	private static final String SQL_UPDATE =
	    		"UPDATE `passenger` SET `cash_account` = ?, `status` = ?, `isBan` = ?, `reputation`= ?, `login` = ?, `password` = ? WHERE `id` = ?";
	private static final String SQL_DELETE =
	    		"DELETE FROM `passenger` WHERE `id` = ?";
	private static final String SQL_FIND_BY_LOGIN_PASSWORD =
			"SELECT `id`,  `cash_account`, `status`, `isBan`, `reputation`,"
			+ " `login`, `password`  FROM `passenger` WHERE  `login` = ? AND `password` = ?";

	
	
	 @Override
	    public Passenger searchUser(String login, String password) throws DAOException {
	        return find(SQL_FIND_BY_LOGIN_PASSWORD, login, password);
	    }

	    /**
	     * Returns the user from the database matching the given SQL query with the given values.
	     * @param sql The SQL query to be executed in the database.
	     * @param login The PreparedStatement value to be set.
	     * @param password PreparedStatement value to be set.
	     * @return The user from the database matching the given SQL query with the given values.
	     * @throws DAOException If something fails at database level.
	     */
	    private Passenger find(String sql, String login, String password) throws DAOException {
	    	PreparedStatement statement = null;
			ResultSet resultSet = null;
			try {
				statement = getConnection().prepareStatement(SQL_FIND_BY_LOGIN_PASSWORD);
				statement.setString(1, login);
				statement.setString(2, password);
				resultSet = statement.executeQuery();
				Passenger user = null;
				if (resultSet.next()) {
					user = new Passenger();
					user.setIdentity(resultSet.getInt("id"));
					user.setLogin(login);
					user.setPassword(password);
					user.setCash(resultSet.getBigDecimal("cash_account"));
					user.setStatus(Category.getByIdentity(resultSet.getInt("status")));
					user.setBan(resultSet.getBoolean("isBan"));
					user.setReputation(resultSet.getInt("reputation"));
				}
				return user;
			} catch (SQLException e) {
				throw new DAOException(e);
			} finally {
				try {
					resultSet.close();
				} catch (SQLException | NullPointerException e) {
				}
				try {
					statement.close();
				} catch (SQLException | NullPointerException e) {
				}
			}
	    }
	

	
	@Override
	public Integer create(Passenger user) throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);
			statement.setInt(1, user.getUser().getIdentity());
			statement.setBigDecimal(2, user.getCash());
			statement.setInt(3, user.getStatus().getIdentity());
			statement.setBoolean(4, user.isBan());
			statement.setInt(5, user.getReputation());
			statement.setString(6, user.getLogin());
			statement.setString(7, user.getPassword());
			statement.executeUpdate();
			resultSet = statement.getGeneratedKeys();
			if (resultSet.next()) {
				return resultSet.getInt(1);
			} else {
				logger.error("There is no index after trying to add record into table `passenger`");
				throw new DAOException(
						"There is no index after trying to add record into table `passenger`");
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				resultSet.close();
			} catch (SQLException | NullPointerException e) {
			}
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}

	@Override
	public Passenger find(Integer id) throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_FIND_BY_ID);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			Passenger user = null;
			if (resultSet.next()) {
				user = new Passenger();
				user.setIdentity(id);
				user.setCash(resultSet.getBigDecimal("cash_account"));
				user.setStatus(Category.getByIdentity(resultSet.getInt("status")));
				user.setBan(resultSet.getBoolean("isBan"));
				user.setReputation(resultSet.getInt("reputation"));
				user.setLogin(resultSet.getString("login"));
				user.setPassword(resultSet.getString("password"));
			}
			return user;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				resultSet.close();
			} catch (SQLException | NullPointerException e) {
			}
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}

	@Override
	public void update(Passenger user) throws DAOException {
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(SQL_UPDATE);

			statement.setInt(1, user.getUser().getIdentity());
			statement.setBigDecimal(2, user.getCash());
			statement.setInt(3, user.getStatus().getIdentity());
			statement.setBoolean(4, user.isBan());
			statement.setInt(5, user.getReputation());
			statement.setString(6, user.getLogin());
			statement.setString(7, user.getPassword());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}

	}

	@Override
	public void delete(Integer identity) throws DAOException {
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(SQL_DELETE);
			statement.setInt(1, identity);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}

	}

	@Override
	public List<Passenger> usersList() throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_LIST_ORDER_BY_ID);
			resultSet = statement.executeQuery();
			List<Passenger> users = new ArrayList<>();
			Passenger user = null;
			while (resultSet.next()) {
				user = new Passenger();
				user.setIdentity(resultSet.getInt("id"));
				user.setCash(resultSet.getBigDecimal("cash_account"));
				user.setStatus(Category.getByIdentity(resultSet.getInt("status")));
				user.setBan(resultSet.getBoolean("isBan"));
				user.setReputation(resultSet.getInt("reputation"));
				user.setLogin(resultSet.getString("login"));
				user.setPassword(resultSet.getString("password"));
				users.add(user);
			}
			return users;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				resultSet.close();
			} catch (SQLException | NullPointerException e) {
			}
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}

}
