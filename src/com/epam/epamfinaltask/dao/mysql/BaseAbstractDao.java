package com.epam.epamfinaltask.dao.mysql;

import java.sql.Connection;

 public abstract class BaseAbstractDao {
	 /**
	     * Mysql Connection object.
	     */
	protected Connection connection;
    /**
     * Gets the connection.
     *
     * @return the connection
     */
    public Connection getConnection() {
        return connection;
    }
    /**
     * Sets the connection.
     *
     * @param dbConnection the connection to set
     */
	public void setConnection(Connection connection) {
		this.connection = connection;
	}
}
