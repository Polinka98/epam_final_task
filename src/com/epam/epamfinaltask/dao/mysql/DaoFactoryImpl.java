package com.epam.epamfinaltask.dao.mysql;

import com.epam.epamfinaltask.controller.InitServlet;
import com.epam.epamfinaltask.dao.DaoFactory;
import com.epam.epamfinaltask.dao.DriverDao;
import com.epam.epamfinaltask.dao.EquipageDao;
import com.epam.epamfinaltask.dao.PassengerDao;
import com.epam.epamfinaltask.dao.RequestDao;
import com.epam.epamfinaltask.dao.TaxiDao;
import com.epam.epamfinaltask.dao.UserDao;
import com.epam.epamfinaltask.dao.pool.ConnectionPool;
import com.epam.epamfinaltask.exception.DAOException;
import com.epam.epamfinaltask.exception.PersistentException;

public class DaoFactoryImpl implements DaoFactory {

	@Override
	public DriverDao getDriverDao() throws DAOException {
		BaseAbstractDao dao = new DriverDaoImpl();
        setDaoConnection(dao);
        return (DriverDao) dao;
	}

	@Override
	public UserDao getUserDao() throws DAOException {
		BaseAbstractDao dao = new UserDaoImpl();
        setDaoConnection(dao);
        return (UserDao) dao;
	}

	@Override
	public PassengerDao getPassengerDao() throws DAOException {
		BaseAbstractDao dao = new PassengerDaoImpl();
        setDaoConnection(dao);
        return (PassengerDao) dao;
	}

	@Override
	public TaxiDao getTaxiDao() throws DAOException {
		BaseAbstractDao dao = new TaxiDaoImpl();
        setDaoConnection(dao);
        return (TaxiDao) dao;
	}

	@Override
	public RequestDao getGequestDao() throws DAOException {
		BaseAbstractDao dao = new RequestDaoImpl();
        setDaoConnection(dao);
        return (RequestDao) dao;
	}

	@Override
	public EquipageDao getEquipageDao() throws DAOException {
		BaseAbstractDao dao = new EquipageDaoImpl();
        setDaoConnection(dao);
        return (EquipageDao) dao;
	}
	 /**
     * Sets dao connection to connection from connection pool.
     * @param dao newly created dao object.
     * @throws DaoException thrown if error occurs while accessing database
     */
    private void setDaoConnection (BaseAbstractDao dao) throws DAOException {
        try {
        	//InitServlet.init();
            dao.setConnection(ConnectionPool.getInstance().getConnection());
        } catch (PersistentException e) {
            throw new DAOException(e);
        }
    }
}
