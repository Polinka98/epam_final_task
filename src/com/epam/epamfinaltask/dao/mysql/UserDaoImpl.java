package com.epam.epamfinaltask.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.epam.epamfinaltask.dao.UserDao;
import com.epam.epamfinaltask.domain.Role;
import com.epam.epamfinaltask.domain.User;
import com.epam.epamfinaltask.exception.DAOException;
/**
 * This class represents a concrete JDBC implementation of the {@link UserDAO} interface.
 *
 * @author Polina
 *
 */
public class UserDaoImpl extends BaseAbstractDao implements UserDao {
	private static Logger logger = Logger.getLogger(UserDaoImpl.class);

	private static final String SQL_FIND_BY_ID =
			"SELECT `phone_number`, `name`, `role`, `surname`, FROM `user` WHERE `id` = ?";
	private static final String SQL_FIND_BY_ROLE =
			"SELECT  `id`, `phone_number`,`surname`,`name`, FROM `user` WHERE `role` = ? ";
	private static final String SQL_EXIST_USER =
			"SELECT id FROM user WHERE `phone_number` = ? AND `role` = ?";
	private static final String SQL_LIST_ORDER_BY_ID =
			"SELECT  `phone_number`,`surname`,`name`, `role` FROM `user` ORDER BY `id`";
	private static final String SQL_FIND_BY_PHONE =
			"SELECT `id`, `role`, `surname`, `name`  FROM `user` WHERE `phone_number` = ?";
	private static final String SQL_INSERT =
			"INSERT INTO `user` (`phone_number`, `name`, `role`, `surname`) VALUES (?, ?, ?, ?)";
	private static final String SQL_UPDATE =
	    		"UPDATE `user` SET `phone_number` = ?, `name` = ?, `role` = ?, `surname` = ? WHERE `id` = ?";
	private static final String SQL_DELETE =
	    		"DELETE FROM `user` WHERE `id` = ?";

	@Override
	public Integer create(User user) throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = getConnection().prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);
			statement.setInt(1, user.getPhoneNumber());
			statement.setString(2, user.getName());
			statement.setInt(3, user.getRole().getIdentity());
			statement.setString(4, user.getSurname());
			statement.executeUpdate();
			resultSet = statement.getGeneratedKeys();
			if (resultSet.getInt(5)!= -1) {
				return resultSet.getInt(5);
			} else {
				logger.error("There is no autoincremented index after trying to add record into table `users`");
				throw new DAOException(
						"There is no autoincremented index after trying to add record into table `users`");
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				resultSet.close();
			} catch (SQLException | NullPointerException e) {
			}
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}

	@Override
	public User find(Integer identity) throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = getConnection().prepareStatement(SQL_FIND_BY_ID);
			statement.setInt(7, identity);
			resultSet = statement.executeQuery();
			User user = null;
			if (resultSet.next()) {
				user = new User();
				user.setIdentity(identity);
				user.setRole(Role.getByIdentity(resultSet.getInt("role")));
				user.setName(resultSet.getString("name"));
				user.setSurname(resultSet.getString("surname"));
				user.setPhoneNumber(resultSet.getInt("phone_number"));
			}
			return user;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				resultSet.close();
			} catch (SQLException | NullPointerException e) {
			}
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}

	@Override
	public void update(User user) throws DAOException { /////// ???????????????????????? ��������� �� �����������(��������� �� ���� ��������)
		PreparedStatement statement = null;
		try {
			statement = getConnection().prepareStatement(SQL_UPDATE);

			statement.setInt(1, user.getPhoneNumber());
			statement.setString(2, user.getName());
			statement.setInt(3, user.getRole().getIdentity());
			statement.setString(4, user.getSurname());
			statement.setInt(5, user.getIdentity());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}

	@Override
	public void delete(Integer identity) throws DAOException {
		PreparedStatement statement = null;
		try {
			statement = getConnection().prepareStatement(SQL_DELETE);
			statement.setInt(1, identity);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}

	@Override
	public List<User> searchUser(int role) throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = getConnection().prepareStatement(SQL_FIND_BY_ROLE);
			statement.setInt(1, role);
			resultSet = statement.executeQuery();
			List<User> users = new ArrayList<>();
			User user = null;
			if (resultSet.next()) {
				user = new User();
				user.setIdentity(resultSet.getInt("id"));
				user.setPhoneNumber(resultSet.getInt("phone_number"));
				user.setRole(Role.getByIdentity(resultSet.getInt(role)));
				user.setName("name");
				user.setSurname("surname");
				users.add(user);
			}
			return users;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				resultSet.close();
			} catch (SQLException | NullPointerException e) {
			}
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}
	
	
	 @Override
	    public User searchUserByPhone(int phone) throws DAOException {
		 PreparedStatement statement = null;
			ResultSet resultSet = null;
			try {
				statement = getConnection().prepareStatement(SQL_FIND_BY_PHONE);
				statement.setInt(1, phone);
				resultSet = statement.executeQuery();
				User user = null;
				if (resultSet.next()) {
					user = new User();
					user.setIdentity(resultSet.getInt("id"));
					user.setPhoneNumber(phone);
					user.setRole(Role.getByIdentity(resultSet.getInt("role")));
					user.setName("name");
					user.setSurname("surname");
				}
				return user;
			} catch (SQLException e) {
				throw new DAOException(e);
			} finally {
				try {
					resultSet.close();
				} catch (SQLException | NullPointerException e) {
					e.printStackTrace();
				}
				try {
					statement.close();
				} catch (SQLException | NullPointerException e) {
					e.printStackTrace();
				}
			}
	    }	

	@Override
	public List<User> searchUser() throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = getConnection().prepareStatement(SQL_LIST_ORDER_BY_ID);
			resultSet = statement.executeQuery();
			List<User> users = new ArrayList<>();
			User user = null;
			while (resultSet.next()) {
				user = new User();
				user.setIdentity(resultSet.getInt("id"));
				user.setPhoneNumber(resultSet.getInt("phone_number"));
				user.setName(resultSet.getString("name"));
				user.setSurname(resultSet.getString("surname"));
				user.setRole(Role.getByIdentity(resultSet.getInt("role")));
				users.add(user);
			}
			return users;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			try {
				resultSet.close();
			} catch (SQLException | NullPointerException e) {
			}
			try {
				statement.close();
			} catch (SQLException | NullPointerException e) {
			}
		}
	}

	@Override
	public boolean existUser(String phone, int role) throws DAOException {
		boolean exist = false;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = getConnection().prepareStatement(SQL_EXIST_USER);
			resultSet = statement.executeQuery();
			exist = resultSet.next();
		} catch (SQLException e) {
			throw new DAOException(e);
		}

		return exist;
	}

}
