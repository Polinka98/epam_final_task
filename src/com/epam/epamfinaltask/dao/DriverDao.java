package com.epam.epamfinaltask.dao;

import java.util.List;

import com.epam.epamfinaltask.domain.Driver;
import com.epam.epamfinaltask.domain.Passenger;
import com.epam.epamfinaltask.exception.DAOException;

/**
 * This interface represents a contract for a DAO for the {@link Driver} model.
 *
 * @author Polina
 */
public interface DriverDao extends Dao<Driver>{
	/**
     * Returns a list of all active drivers from the database ordered by user ID. The list is never null and
     * is empty when the database does not contain any user.
     * @return A list of all drivers from the database ordered by user ID.
     * @throws DAOException If something fails at database level.
     */
	List<Driver> searchActiveDriver() throws DAOException;
	/**
     * Returns a list of all free and active drivers from the database ordered by user ID. The list is never null and
     * is empty when the database does not contain any user.
     * @return A list of all drivers from the database ordered by user ID.
     * @throws DAOException If something fails at database level.
     */
	List<Driver> searchFreeActiveDriver() throws DAOException;
	/**
     * Returns a list of all drivers from the database ordered by user ID. The list is never null and
     * is empty when the database does not contain any user.
     * @return A list of all drivers from the database ordered by user ID.
     * @throws DAOException If something fails at database level.
     */
	List<Driver> driversList() throws DAOException;
	/**
     * Returns the user from the database matching the given login and password, otherwise null.
     * @param login The email of the user to be returned.
     * @param password The password of the user to be returned.
     * @return The user from the database matching the given email and password, otherwise null.
     * @throws DAOException If something fails at database level.
     */
	Driver searchDriver(String login, String password) throws DAOException;
}
