package com.epam.epamfinaltask.dao;

import java.util.List;

import com.epam.epamfinaltask.domain.User;
import com.epam.epamfinaltask.exception.DAOException;

/**
 * This interface represents a contract for a DAO for the {@link User} model.
 * Note that all methods which returns the {@link User} from the DB, will not
 * fill the model with the password, due to security reasons.
 *
 * @author Polina
 */
public interface UserDao extends Dao<User> {
	/**
     * Returns  a list of all users from the database matching the given role, otherwise null.
     * @param role The role of the user to be returned.
     * @return A list of all users from the database matching the given role, otherwise null.
     * @throws DAOException If something fails at database level.
     */
	List<User> searchUser(int role) throws DAOException;
	/**
     * Returns the user from the database matching the given phone and password, otherwise null.
     * @param phone The phone of the user to be returned.
     * @param password The password of the user to be returned.
     * @return The user from the database matching the given email and password, otherwise null.
     * @throws DAOException If something fails at database level.
     */
	User searchUserByPhone(int phone) throws DAOException;
	 /**
     * Returns a list of all users from the database ordered by user ID. The list is never null and
     * is empty when the database does not contain any user.
     * @return A list of all users from the database ordered by user ID.
     * @throws DAOException If something fails at database level.
     */
	List<User> searchUser() throws DAOException;
	
	/**
     * Returns true if the given phone exist in the database in conjunction with role.
     * @param phone The phone which is to be checked in the database.
     * @param role The role which is to be checked in the database.
     * @return True if the given phone exist in the database.
     * @throws DAOException If something fails at database level.
     */
     boolean existUser(String phone, int role) throws DAOException;
}
