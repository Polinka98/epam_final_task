package com.epam.epamfinaltask.dao;

import java.util.List;

import com.epam.epamfinaltask.domain.Taxi;
import com.epam.epamfinaltask.exception.DAOException;
/**
 * This interface represents a contract for a DAO for the {@link Taxi} model.
 *
 * @author Polina
 */
public interface TaxiDao extends Dao<Taxi> {
	/**
     * Returns a list of all free taxis from the database ordered by user ID. The list is never null and
     * is empty when the database does not contain any user.
     * @return A list of all free taxis from the database ordered by user ID.
     * @throws DAOException If something fails at database level.
     */

	List<Taxi> searchTaxi() throws DAOException;
	/**
     * Returns a list of all taxis from the database ordered by user ID. The list is never null and
     * is empty when the database does not contain any user.
     * @return A list of all taxis from the database ordered by user ID.
     * @throws DAOException If something fails at database level.
     */
	List<Taxi> taxiList() throws DAOException;
	
}
