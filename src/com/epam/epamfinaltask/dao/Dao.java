package com.epam.epamfinaltask.dao;

import com.epam.epamfinaltask.domain.Entity;
import com.epam.epamfinaltask.exception.DAOException;
/**
 * This interface represents a contract for a DAO for the {@link Entity} model.
 * Note that all methods which returns the {@link User} from the DB, will not
 * fill the model with the password, due to security reasons.
 *
 * @author Polina
 */
public interface Dao<Type extends Entity> {
	/**
     * Create the given entity in the database. 
     * @param entity The entity to be created in the database.
     * @throws DAOException If something fails at database level.
     */
	Integer create(Type entity) throws DAOException;
	/**
     * Find the given entity by id in the database. 
     * @param identity The entity to be found in the database.
     * @throws DAOException If something fails at database level.
     */
	Type find(Integer identity) throws DAOException;

    /**
     * Update the given entity in the database. 
     * @param entity The entity to be updated in the database.
     * @throws DAOException If something fails at database level.
     */
	void update(Type entity) throws DAOException;
	/**
     * Delete the given entity by id from the database. After deleting, the DAO will set the ID of the given
     * entity to null.
     * @param identity the entity to be deleted from the database.
     * @throws DAOException If something fails at database level.
     */
	void delete(Integer identity) throws DAOException;
	
}
