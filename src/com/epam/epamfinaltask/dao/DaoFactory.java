package com.epam.epamfinaltask.dao;

import com.epam.epamfinaltask.exception.DAOException;

/**
 * Provides interface for creating dao objects.
 */
public interface DaoFactory {

    /**
     * Creates Driver Dao object.
     * @return Driver dao object.
     * @throws DaoException thrown if error occurs while accessing database
     */
    DriverDao getDriverDao() throws DAOException;

    /**
     * Creates User Dao object.
     * @return User dao object.
     * @throws DaoException thrown if error occurs while accessing database
     */
    UserDao getUserDao() throws DAOException;

    /**
     * Creates Passenger Dao object.
     * @return Passenger dao object.
     * @throws DaoException thrown if error occurs while accessing database
     */
    PassengerDao getPassengerDao() throws DAOException;

    /**
     * Creates Taxi Dao object.
     * @return Taxi dao object.
     * @throws DaoException thrown if error occurs while accessing database
     */
    TaxiDao getTaxiDao() throws DAOException;

    /**
     * Creates Request Dao object.
     * @return Request dao object.
     * @throws DaoException thrown if error occurs while accessing database
     */
    RequestDao getGequestDao() throws DAOException;

    /**
     * Creates Equipage Dao object.
     * @return Equipage dao object.
     * @throws DaoException thrown if error occurs while accessing database
     */
    EquipageDao getEquipageDao() throws DAOException;
}
