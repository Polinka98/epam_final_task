package com.epam.epamfinaltask.dao;

import java.util.List;

import com.epam.epamfinaltask.domain.Equipage;
import com.epam.epamfinaltask.exception.DAOException;
/**
 * This interface represents a contract for a DAO for the {@link Equipage} model.
 *
 * @author Polina
 */
public interface EquipageDao extends Dao<Equipage> {
	/**
     * Returns  equipage from the database matching the given driverId, otherwise null.
     * @param driverId The driverId of the driver to be returned.
     * @return equipage  from the database matching the given driverId, otherwise null.
     * @throws DAOException If something fails at database level.
     */
	Equipage searchEquipageByDriverId(int driverId) throws DAOException;
	/**
     * Returns a list of all free equipages from the database ordered by equipage ID. The list is never null and
     * is empty when the database does not contain any user.
     * @return A list of all free equipages from the database ordered by equipage ID.
     * @throws DAOException If something fails at database level.
     */
	List<Equipage> searchFreeEquipage() throws DAOException;
	/**
     * Returns a list of all equipages from the database ordered by equipage ID. The list is never null and
     * is empty when the database does not contain any user.
     * @return A list of all equipages from the database ordered by equipage ID.
     * @throws DAOException If something fails at database level.
     */
	List<Equipage> equipageList() throws DAOException;
}
