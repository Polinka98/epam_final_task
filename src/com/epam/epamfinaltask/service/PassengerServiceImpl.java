package com.epam.epamfinaltask.service;

import java.util.List;

import com.epam.epamfinaltask.domain.Passenger;
import com.epam.epamfinaltask.exception.DAOException;
import com.epam.epamfinaltask.exception.ServiceException;
import com.epam.epamfinaltask.helper.Cryptographer;

public class PassengerServiceImpl extends AbstractService implements PassengerService {

	@Override
	public List<Passenger> usersList() throws ServiceException {
		try  {
			return daoFactory.getPassengerDao().usersList(); }
		catch (final DAOException e) {
            throw new ServiceException(
                    "Cannot access database: " + e.getMessage(), e);
        }
	}

	@Override
	public Passenger searchUser(String login, String password) throws ServiceException {
		try  {
			return daoFactory.getPassengerDao().searchUser(login, password); }
		catch (final DAOException e) {
            throw new ServiceException(
                    "Cannot access database: " + e.getMessage(), e);
        }
	}

	@Override
	public void save(Passenger user) throws ServiceException  {
		try {
			if(user.getUser().getIdentity() != null) {
			if(user.getPassword() != null) {
				user.setPassword(Cryptographer.md5(user.getPassword()));
			} 
//				else {
//				Passenger oldUser = dao.find(user.getIdentity());
//				user.setPassword(oldUser.getPassword());
//			}
				daoFactory.getPassengerDao().update(user);
		} else {
			user.setPassword(Cryptographer.md5(new String()));
			user.setIdentity(daoFactory.getPassengerDao().create(user));
		}
	        } catch (final DAOException e) {
	        	throw new ServiceException(
	                    "Cannot access database: " + e.getMessage(), e);
	        }
	}

	@Override
	public void delete(Integer identity) throws ServiceException  {
		try  {
			daoFactory.getPassengerDao().delete(identity);; }
		catch (final DAOException e) {
            throw new ServiceException(
                    "Cannot access database: " + e.getMessage(), e);
        }		
	}

	@Override
	public Passenger findByIdentity(Integer identity) throws ServiceException {
		try  {
			return daoFactory.getPassengerDao().find(identity); }
		catch (final DAOException e) {
            throw new ServiceException(
                    "Cannot access database: " + e.getMessage(), e);
        }
	}

	 /**
     * Creates user entity in table
     * @param user user entity
     * @return operation result
     * @throws ServiceException thrown in case of error while accessing dao
     */
    public boolean create(Passenger user) throws ServiceException {
		return false;
//        try {
//            return daoFactory.getUserDao().create(user);
//        } catch (DaoException e) {
//            throw new ServiceException(e);
//        }
    }
	
}
