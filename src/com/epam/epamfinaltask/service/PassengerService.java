package com.epam.epamfinaltask.service;

import java.util.List;

import com.epam.epamfinaltask.domain.Passenger;
import com.epam.epamfinaltask.exception.DAOException;
import com.epam.epamfinaltask.exception.ServiceException;
/**
 * This interface represents a contract for a service for the {@link Passenger} model.
 *
 * @author Polina
 */
public interface PassengerService extends Service {

	 /**
     * Returns a list of all simple users from the database ordered by user ID. The list is never null and
     * is empty when the database does not contain any user.
     * @return A list of all simple users from the database ordered by user ID.
     * @throws DAOException If something fails at database level.
     */
	List<Passenger> usersList() throws ServiceException;
	/**
     * Returns the user from the database matching the given login and password, otherwise null.
     * @param login The email of the user to be returned.
     * @param password The password of the user to be returned.
     * @return The user from the database matching the given email and password, otherwise null.
     * @throws DAOException If something fails at database level.
     */
	Passenger searchUser(String login, String password) throws ServiceException;
	/**
     * Update(save) the given entity in the database. 
     * @param entity The entity to be updated in the database.
     * @throws DAOException If something fails at database level.
     */
	void save(Passenger user) throws ServiceException;
	/**
     * Delete the given entity by id from the database. After deleting, the DAO will set the ID of the given
     * entity to null.
     * @param identity the entity to be deleted from the database.
     * @throws DAOException If something fails at database level.
     */
	void delete(Integer identity) throws ServiceException;
	/**
     * Find the given entity by id in the database. 
     * @param identity The entity to be found in the database.
     * @throws DAOException If something fails at database level.
     */
	Passenger findByIdentity(Integer identity) throws ServiceException;
}
