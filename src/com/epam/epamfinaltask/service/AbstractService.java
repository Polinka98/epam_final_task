package com.epam.epamfinaltask.service;

import com.epam.epamfinaltask.dao.DaoFactory;
import com.epam.epamfinaltask.dao.mysql.DaoFactoryImpl;

/**
 * Abstract service class.
 */
public abstract class AbstractService {

    /**
     * Implemented dao factory object.
     */
    DaoFactory daoFactory = new DaoFactoryImpl();

}
