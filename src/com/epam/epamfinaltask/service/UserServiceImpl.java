package com.epam.epamfinaltask.service;

import java.util.List;

import com.epam.epamfinaltask.domain.User;
import com.epam.epamfinaltask.exception.DAOException;
import com.epam.epamfinaltask.exception.ServiceException;

public class UserServiceImpl extends AbstractService implements UserService{
	@Override
	public List<User> findAll() throws ServiceException {
		try  {
			return daoFactory.getUserDao().searchUser(); }
		catch (final DAOException e) {
            throw new ServiceException(
                    "Cannot access database: " + e.getMessage(), e);
        }
	}

	@Override
	public boolean existenceOfUser(String phone, int role) throws ServiceException  {
		try  {
			return daoFactory.getUserDao().existUser(phone, role); }
		catch (final DAOException e) {
            throw new ServiceException(
                    "Cannot access database: " + e.getMessage(), e);
        }
	}

	@Override
	public User findByIdentity(Integer identity) throws ServiceException  {
		try  {
			return daoFactory.getUserDao().find(identity); }
		catch (final DAOException e) {
            throw new ServiceException(
                    "Cannot access database: " + e.getMessage(), e);
        }
	}
	
	@Override
	public User searchUserByPhone(int phone) throws ServiceException  {
		try  {
			return daoFactory.getUserDao().searchUserByPhone(phone); }
		catch (final DAOException e) {
            throw new ServiceException(
                    "Cannot access database: " + e.getMessage(), e);
        }
	}

	@Override
	public List<User> findByRole(int role) throws ServiceException  {
		try  {
			return daoFactory.getUserDao().searchUser(role); }
		catch (final DAOException e) {
            throw new ServiceException(
                    "Cannot access database: " + e.getMessage(), e);
        }
	}

	@Override
	public void delete(Integer identity) throws ServiceException  {
		try  {
			daoFactory.getUserDao().delete(identity);
	        } catch (final DAOException e) {
	            throw new ServiceException(
	                    "Cannot access database: " + e.getMessage(), e);
	        }
	}

}
