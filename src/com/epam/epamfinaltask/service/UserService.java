package com.epam.epamfinaltask.service;

import java.util.List;

import com.epam.epamfinaltask.domain.User;
import com.epam.epamfinaltask.exception.DAOException;
import com.epam.epamfinaltask.exception.ServiceException;
/**
 * This interface represents a contract for a Service for the {@link User} model.
 *
 * @author Polina
 */
public interface UserService extends Service {
	/**
     * Returns true if the given phone exist in the database in conjunction with role.
     * @param phone The phone which is to be checked in the database.
     * @param role The role which is to be checked in the database.
     * @return True if the given phone exist in the database.
     * @throws DAOException If something fails at database level.
     */
	boolean existenceOfUser(String phone, int role) throws ServiceException;
	/**
     * Returns  a list of all users from the database matching the given role, otherwise null.
     * @param role The role of the user to be returned.
     * @return A list of all users from the database matching the given role, otherwise null.
     * @throws DAOException If something fails at database level.
     */
	List<User> findByRole(int role) throws ServiceException;
	/**
     * Returns a list of all users from the database ordered by user ID. The list is never null and
     * is empty when the database does not contain any user.
     * @return A list of all users from the database ordered by user ID.
     * @throws DAOException If something fails at database level.
     */
	List<User> findAll() throws ServiceException;
	/**
     * Find the given entity by id in the database. 
     * @param identity The entity to be found in the database.
     * @throws DAOException If something fails at database level.
     */
	User findByIdentity(Integer identity) throws ServiceException;

	/**
     * Returns the user from the database matching the given phone and password, otherwise null.
     * @param phone The phone of the user to be returned.
     * @param password The password of the user to be returned.
     * @return The user from the database matching the given email and password, otherwise null.
     * @throws DAOException If something fails at database level.
     */	
	User searchUserByPhone(int phone) throws ServiceException;

	/**
     * Delete the given entity by id from the database. After deleting, the DAO will set the ID of the given
     * entity to null.
     * @param identity the entity to be deleted from the database.
     * @throws DAOException If something fails at database level.
     */
	void delete(Integer identity) throws ServiceException;
}
