package com.epam.epamfinaltask.service;

import java.util.List;

import com.epam.epamfinaltask.domain.Driver;
import com.epam.epamfinaltask.exception.DAOException;
import com.epam.epamfinaltask.exception.ServiceException;
import com.epam.epamfinaltask.helper.Cryptographer;

public class DriverServiceImpl extends AbstractService implements DriverService {

	@Override
	public List<Driver> driversList() throws ServiceException {
		try  {
			return daoFactory.getDriverDao().driversList(); }
		catch (final DAOException e) {
            throw new ServiceException(
                    "Cannot access database: " + e.getMessage(), e);
        }
	}

	@Override
	public Driver searchDriver(String login, String password) throws ServiceException {
		try  {
			return daoFactory.getDriverDao().searchDriver(login, password); }
		catch (final DAOException e) {
            throw new ServiceException(
                    "Cannot access database: " + e.getMessage(), e);
        }
	}

	@Override
	public void save(Driver user) throws ServiceException {
		try  {
			if(user.getIdentity() != null) {
			if(user.getPassword() != null) {
				user.setPassword(Cryptographer.md5(user.getPassword()));
			} 
			//else {
//				Driver oldUser = dao.find(user.getIdentity());
//				user.setPassword(oldUser.getPassword());
//			}
				daoFactory.getDriverDao().update(user);
		} else {
			user.setPassword(Cryptographer.md5(new String()));
			user.setIdentity(daoFactory.getDriverDao().create(user));
		}
	        } catch (final DAOException e) {
	            throw new ServiceException(
	                    "Cannot access database: " + e.getMessage(), e);
	        }		
	}

	@Override
	public void delete(Integer identity) throws ServiceException {
		try  {
			daoFactory.getDriverDao().delete(identity); }
		catch (final DAOException e) {
            throw new ServiceException(
                    "Cannot access database: " + e.getMessage(), e);
        }
	}

	@Override
	public Driver findByIdentity(Integer identity) throws ServiceException {
		try  {
			return daoFactory.getDriverDao().find(identity); }
		catch (final DAOException e) {
            throw new ServiceException(
                    "Cannot access database: " + e.getMessage(), e);
        }
	}

	@Override
	public List<Driver> searchActiveDriver() throws ServiceException {
		try  {
			return daoFactory.getDriverDao().searchActiveDriver(); }
		catch (final DAOException e) {
            throw new ServiceException(
                    "Cannot access database: " + e.getMessage(), e);
        }
	}

	@Override
	public List<Driver> searchFreeActiveDriver() throws ServiceException {
		try  {
			return daoFactory.getDriverDao().searchFreeActiveDriver(); }
		catch (final DAOException e) {
            throw new ServiceException(
                    "Cannot access database: " + e.getMessage(), e);
        }
	}
	public boolean create(Driver user) throws ServiceException {
		return false;
//        try {
//            return daoFactory.getUserDao().create(user);
//        } catch (DaoException e) {
//            throw new ServiceException(e);
        }
	
}
