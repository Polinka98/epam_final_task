package com.epam.epamfinaltask.domain;

import java.util.HashMap;
import java.util.Map;

public enum Category {
	/** Status - beginner, sale value - 1%. **/
	BEGINNER("beginner"),
	/** Status - regular, sale value - 5%. **/
	REGULAR("regular"),
	/** Status - vip, sale value - 11%. **/
	VIP("vip");

	/** Value. **/
	private String value;

	public Integer getIdentity() {
		return ordinal();
	}

	public static Category getByIdentity(Integer identity) {
		return Category.values()[identity];
	}

	/**
	 * Constructs parsing constant with the given string value.
	 *
	 * @param value1 the value to set.
	 */
	Category(final String value1) {
		this.value = value1;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/** Map of values. **/
	private static final Map<String, Category> VALUES;

	static {
		VALUES = new HashMap<>();
		for (Category enumValue : Category.values()) {
			VALUES.put(enumValue.value, enumValue);
		}
	}

	/**
	 * @param value1 string value
	 * @return returns enum value that corresponds to given {@code value1}
	 */
	public static Category getEnumValue(final String value1) {
		return VALUES.get(value1);
	}
}
