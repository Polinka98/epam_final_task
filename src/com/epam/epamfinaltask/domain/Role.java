package com.epam.epamfinaltask.domain;

public enum Role {
	ADMINISTRATOR("Administrator"),
	DRIVER("Driver"),
	USER("User");

	private String role;

	private Role(String name) {
		this.role = name;
	}

	public String getName() {
		return role;
	}

	public Integer getIdentity() {
		return ordinal();
	}

	public static Role getByIdentity(Integer identity) {
		return Role.values()[identity];
	}
}
