package com.epam.epamfinaltask.domain;

import java.io.Serializable;
/**
*
* @author Polina
*
*/
abstract public class Entity implements Serializable  {

	private static final long serialVersionUID = 875272064312010858L;
	/**Identity.**/
	private Integer identity;
/**
 * @return identity
 */
	public Integer getIdentity() {
		return identity;
	}
/**
 * @param identity
 */
	public void setIdentity(Integer identity) {
		this.identity = identity;
	}

	 /**
     * {@inheritDoc}
     */
	@Override
	public boolean equals(Object object) {
		if(object != null) {
			if(object != this) {
				if(object.getClass() == getClass() && identity != null) {
					return identity.equals(((Entity)object).identity);
				}
				return false;
			}
			return true;
		}
		return false;
	}

	 /**
     * {@inheritDoc}
     */
	@Override
	public int hashCode() {
		return identity != null ? identity.hashCode() : 0;
	}

}
