package com.epam.epamfinaltask.domain;

/**
 *
 * @author Polina
 *
 */
public class Equipage extends Entity {
	/**Driver's id.**/
	private int driverId;
	/**Taxi's id.**/
	private int taxiId;
	/**Waiting for request.**/
	private boolean isFree;
	private double currentCoordLond;
	private double currentCoordLat;

	public int getDriverId() {
		return driverId;
	}
	public void setDriverId(int driverId) {
		this.driverId = driverId;
	}
	public int getTaxiId() {
		return taxiId;
	}
	public void setTaxiId(int taxiId) {
		this.taxiId = taxiId;
	}

	public double getCurrentCoordLond() {
		return currentCoordLond;
	}
	public void setCurrentCoordLond(double currentCoordLond) {
		this.currentCoordLond = currentCoordLond;
	}
	public double getCurrentCoordLat() {
		return currentCoordLat;
	}
	public void setCurrentCoordLat(double currentCoordLat) {
		this.currentCoordLat = currentCoordLat;
	}
	/**
	 * @return isFree waiting for request
	 */
	public boolean getFree() {
		return isFree;
	}
	/**
	 * @param isFree1 new state
	 */
	public void setFree(final boolean isFree1) {
		this.isFree = isFree1;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		long temp;
		temp = Double.doubleToLongBits(currentCoordLat);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(currentCoordLond);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + driverId;
		result = prime * result + (isFree ? 1231 : 1237);
		result = prime * result + taxiId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Equipage other = (Equipage) obj;
		if (Double.doubleToLongBits(currentCoordLat) != Double.doubleToLongBits(other.currentCoordLat))
			return false;
		if (Double.doubleToLongBits(currentCoordLond) != Double.doubleToLongBits(other.currentCoordLond))
			return false;
		if (driverId != other.driverId)
			return false;
		if (isFree != other.isFree)
			return false;
		if (taxiId != other.taxiId)
			return false;
		return true;
	}
	/**
	 * {@inheritDoc}.
	 */
	@Override
	public String toString() {
		return "Equipage [driverId=" + driverId + ", taxiId=" + taxiId + ", isFree=" + isFree + "]";
	}

}
