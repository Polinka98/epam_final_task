package com.epam.epamfinaltask.domain;

abstract public class GeneralInfo extends Entity {
	private String login;
	private String password;
	private User user;
	private int reputation;
	private boolean ban;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	/**
     * @return login user's email
     */
	public String getLogin() {
		return login;
	}
	/**
	   * @param  login1 new email
	   */
	public void setLogin(String login1) {
		this.login = login1;
	}
	/**
     * @return password user's password
     */
	public String getPassword() {
		return password;
	}
	 /**
	   * @param password1 new password
	   */
	public void setPassword(String password1) {
		this.password = password1;
	}

	/**
	 * @return reputation user's reputation according to marks
	 */
	public int getReputation() {
		return reputation;
	}

	/**
	 * @param reputation1 new reputation
	 */
	public void setReputation(int reputation1) {
		this.reputation = reputation1;
	}

	/**
	 * @return isBan user's state
	 */
	public boolean isBan() {
		return ban;
	}

	/**
	 * @param ban1 new state
	 */
	public void setBan(boolean ban1) {
		this.ban = ban1;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (ban ? 1231 : 1237);
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + reputation;
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GeneralInfo other = (GeneralInfo) obj;
		if (ban != other.ban)
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (reputation != other.reputation)
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "GeneralInfo [login=" + login + ", password=" + password + ", user=" + user + ", reputation="
				+ reputation + ", ban=" + ban + "]";
	}

}
