package com.epam.epamfinaltask.domain;

/**
 *
 * @author Polina
 *
 */
public class Driver extends GeneralInfo {

	/** Driver ready to work. **/
	private boolean isActive;
	/** Waiting for request. **/
	private boolean isFree;

	/**
	 * @return isActive driver is ready to work
	 */
	public boolean getActive() {
		return isActive;
	}

	/**
	 * @param isActive1 new state
	 */
	public void setActive(final boolean isActive1) {
		this.isActive = isActive1;
	}

	/**
	 * @return isFree driver waiting for request
	 */
	public boolean getFree() {
		return isFree;
	}

	/**
	 * @param isFree1 new state
	 */
	public void setFree(final boolean isFree1) {
		this.isFree = isFree1;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (isActive ? 1231 : 1237);
		result = prime * result + (isFree ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Driver other = (Driver) obj;
		if (isActive != other.isActive)
			return false;
		if (isFree != other.isFree)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Driver [isActive=" + isActive + ", isFree=" + isFree + "]";
	}

}
