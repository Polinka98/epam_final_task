package com.epam.epamfinaltask.domain;

import java.util.Optional;

/**
 *
 * @author Polina
 *
 */
public class Taxi extends Entity {
	/** Auto number. **/
	private String autoNumber;
	/** Waiting for request. **/
	private boolean isFree;
	/** Auto photo. **/
	private Optional<String> image = Optional.empty();

	/**
	 * @param image new photo
	 */
	public void setImage(String image) {
		Optional.ofNullable(image);
	}

	/**
	 * @return image photo
	 */
	public Optional<String> getImage() {
		return image;
	}

	public String getAutoNumber() {
		return autoNumber;
	}

	/**
	 * @param autoNumber1 new id
	 */
	public void setAutoNumber(final String autoNumber1) {
		this.autoNumber = autoNumber1;
	}

	/**
	 * @return isFree waiting for request
	 */
	public boolean getFree() {
		return isFree;
	}

	/**
	 * @param isFree1 new state
	 */
	public void setFree(final boolean isFree1) {
		this.isFree = isFree1;
	}

	/**
	 * {@inheritDoc}.
	 */
	@Override
	public String toString() {
		return "Taxi [autoNumber=" + autoNumber + ", isFree=" + isFree + ", image=" + image + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((autoNumber == null) ? 0 : autoNumber.hashCode());
		result = prime * result + ((image == null) ? 0 : image.hashCode());
		result = prime * result + (isFree ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Taxi other = (Taxi) obj;
		if (autoNumber == null) {
			if (other.autoNumber != null)
				return false;
		} else if (!autoNumber.equals(other.autoNumber))
			return false;
		if (image == null) {
			if (other.image != null)
				return false;
		} else if (!image.equals(other.image))
			return false;
		if (isFree != other.isFree)
			return false;
		return true;
	}
}
