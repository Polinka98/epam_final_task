package com.epam.epamfinaltask.domain;

/**
 *
 * @author Polina
 *
 */
public class User extends Person {
	/** Foreign key for driver a. **/
	private int phoneNumber;
	/** User's role. **/
	private Role role;

	public User(final int id1, final String name, final String surname, final int number, final String password1,
			final String login1, final Role role1) {
		this.setIdentity(id1);
		this.setName(name);
		this.setSurname(surname);
		this.phoneNumber = number;
		this.role = role1;
	}

	public User() {

	}

	/**
	 * @return phoneNumber user's id
	 */
	public int getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber1 new number
	 */
	public void setPhoneNumber(final int phoneNumber1) {
		this.phoneNumber = phoneNumber1;
	}

	/**
	 * @return role user's role
	 */
	public Role getRole() {
		return role;
	}

	/**
	 * @param role1 user's new roles
	 */
	public void setRole(Role role1) {
		this.role = role1;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + phoneNumber;
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (phoneNumber != other.phoneNumber)
			return false;
		if (role != other.role)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "User [phoneNumber=" + phoneNumber + ", role=" + role + "]";
	}
}
