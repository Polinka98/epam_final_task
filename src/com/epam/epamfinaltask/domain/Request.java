package com.epam.epamfinaltask.domain;

import java.math.BigDecimal;
import java.util.Date;


public class Request extends Entity {
	private String userId;
	private int equipageId;
	private BigDecimal price;
	private boolean payment;
	private boolean archive;
	private int markForUser;
	private int markForDriver;
	private double userCurrentCoordLond;
	private double userCurrentCoordLat;
	private double finalCoordLong;
	private double finalCoordLat;
	private Date dataTimeOfRequest;
	private Date timeOfShipping;
	private Date timeOfArrival;

	public Date getDataTimeOfRequest() {
		return dataTimeOfRequest;
	}
	public void setDataTimeOfRequest(Date dataTimeOfRequest) {
		this.dataTimeOfRequest = dataTimeOfRequest;
	}
	public Date getTimeOfShipping() {
		return timeOfShipping;
	}
	public void setTimeOfShipping(Date timeOfShipping) {
		this.timeOfShipping = timeOfShipping;
	}
	public Date getTimeOfArrival() {
		return timeOfArrival;
	}
	public void setTimeOfArrival(Date timeOfArrival) {
		this.timeOfArrival = timeOfArrival;
	}

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getEquipageId() {
		return equipageId;
	}
	public void setEquipageId(int equipageId) {
		this.equipageId = equipageId;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public boolean isPayment() {
		return payment;
	}
	public void setPayment(boolean payment) {
		this.payment = payment;
	}
	public boolean isArchive() {
		return archive;
	}
	public void setArchive(boolean archive) {
		this.archive = archive;
	}
	public int getMarkForUser() {
		return markForUser;
	}
	public void setMarkForUser(int markForUser) {
		this.markForUser = markForUser;
	}
	public int getMarkForDriver() {
		return markForDriver;
	}
	public void setMarkForDriver(int markForDriver) {
		this.markForDriver = markForDriver;
	}
	public double getUserCurrentCoordLond() {
		return userCurrentCoordLond;
	}
	public void setUserCurrentCoordLond(double userCurrentCoordLond) {
		this.userCurrentCoordLond = userCurrentCoordLond;
	}
	public double getUserCurrentCoordLat() {
		return userCurrentCoordLat;
	}
	public void setUserCurrentCoordLat(double userCurrentCoordLat) {
		this.userCurrentCoordLat = userCurrentCoordLat;
	}
	public double getFinalCoordLong() {
		return finalCoordLong;
	}
	public void setFinalCoordLong(double finalCoordLong) {
		this.finalCoordLong = finalCoordLong;
	}
	public double getFinalCoordLat() {
		return finalCoordLat;
	}
	public void setFinalCoordLat(double finalCoordLat) {
		this.finalCoordLat = finalCoordLat;
	}

	@Override
	public String toString() {
		return "Request [userId=" + userId + ", equipageId=" + equipageId + ", price=" + price
				+ ", markForUser=" + markForUser + ", markForDriver=" + markForDriver + ", dataTimeOfRequest="
				+ dataTimeOfRequest + "]";
	}
	
	

}
