package com.epam.epamfinaltask.domain;

import java.math.BigDecimal;

/**
 *
 * @author Polina
 *
 */
public class Passenger extends GeneralInfo {

	/** User's status. **/
	private Category status;
	/** Cash value. **/
	private BigDecimal cash;

	public Category getStatus() {
		return status;
	}

	public void setStatus(Category status) {
		this.status = status;
	}

	/**
	 * @return cash user's cash
	 */
	public BigDecimal getCash() {
		return cash;
	}

	/**
	 * @param cash1 new value
	 */
	public void setCash(final BigDecimal cash1) {
		this.cash = cash1;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((cash == null) ? 0 : cash.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Passenger other = (Passenger) obj;
		if (cash == null) {
			if (other.cash != null)
				return false;
		} else if (!cash.equals(other.cash))
			return false;
		if (status != other.status)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Passenger [status=" + status + ", cash=" + cash + "]";
	}

}
