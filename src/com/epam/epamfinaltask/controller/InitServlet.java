package com.epam.epamfinaltask.controller;

import java.io.IOException;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Layout;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

import com.epam.epamfinaltask.dao.pool.ConnectionPool;
import com.epam.epamfinaltask.exception.PersistentException;

public class InitServlet {

	public static final String LOG_FILE_NAME = "log.txt";
	public static final Level LOG_LEVEL = Level.ALL;
	public static final String LOG_MESSAGE_FORMAT = "%n%d%n%p\t%C.%M:%L%n%m%n";

	public static final String DB_DRIVER_CLASS = "com.mysql.jdbc.Driver";
	public static final String DB_URL ="jdbc:mysql://localhost:3306/buber"+
          "?verifyServerCertificate=false"+
          "&useSSL=false"+
          "&requireSSL=false"+
          "&useLegacyDatetimeCode=false"+
          "&amp"+
          "&serverTimezone=UTC";// "jdbc:mysql://localhost:3306/buber?useUnicode=true&characterEncoding=UTF-8";
	public static final String DB_USER = "root";
	public static final String DB_PASSWORD = "root";
	public static final int DB_POOL_START_SIZE = 10;
	public static final int DB_POOL_MAX_SIZE = 1000;
	public static final int DB_POOL_CHECK_CONNECTION_TIMEOUT = 0;
	
	public static void init() throws PersistentException {
		try {
//			Logger root = Logger.getRootLogger();
//			Layout layout = new PatternLayout(LOG_MESSAGE_FORMAT);
//			root.addAppender(new FileAppender(layout, LOG_FILE_NAME, true));
//			root.addAppender(new ConsoleAppender(layout));
//			root.setLevel(LOG_LEVEL);
			ConnectionPool.getInstance().init(DB_DRIVER_CLASS, DB_URL, DB_USER, DB_PASSWORD, DB_POOL_START_SIZE, DB_POOL_MAX_SIZE, DB_POOL_CHECK_CONNECTION_TIMEOUT);
		} catch(PersistentException e) {
			throw new PersistentException();
		}
	}
}
