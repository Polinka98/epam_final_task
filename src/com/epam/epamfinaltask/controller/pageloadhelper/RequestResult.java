package com.epam.epamfinaltask.controller.pageloadhelper;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Provides interface for page loading.
 */
public interface RequestResult {
	 /**
     * Executes the action which is the result of processing request.
     *
     * @param  request          the request
     * @param  response         the response
     * @throws ServletException the servlet exception
     * @throws IOException      Signals that an I/O exception has occurred.
     */
    void execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException;
}
