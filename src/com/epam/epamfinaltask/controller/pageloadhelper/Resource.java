package com.epam.epamfinaltask.controller.pageloadhelper;

/**
 * Subclasses use {@code url} value to store URL for forward or redirect command.
 */
public abstract class Resource implements RequestResult {

	  /**
     * Loaded page URL.
     */
    private String url;

    /**
     * Instantiates a new abstract page loader from page url.
     *
     * @param pageUrl the page url
     */
    public Resource(final String pageUrl) {
        url = pageUrl;
    }

    /**
     * Gets the url.
     *
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the url.
     *
     * @param pageUrl the url to set
     */
    public void setUrl(final String pageUrl) {
        url = pageUrl;
    }
}
