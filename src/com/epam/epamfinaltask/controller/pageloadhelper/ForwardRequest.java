package com.epam.epamfinaltask.controller.pageloadhelper;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * Forward request and response.
 */
public class ForwardRequest extends Resource{
	/**
    * Instantiates a new forward page loader.
    *
    * @param url the url to forward to
    */
	public ForwardRequest(String pageUrl) {
		super(pageUrl);
	}
	/**
     * {@inheritDoc}
     */
    @Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 request.getRequestDispatcher(getUrl()).forward(request, response);
	}
}
