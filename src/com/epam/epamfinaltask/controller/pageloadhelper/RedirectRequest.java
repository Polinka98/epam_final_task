package com.epam.epamfinaltask.controller.pageloadhelper;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * Redirects request to current URL.
 */
public class RedirectRequest extends Resource {

    /**
     * Instantiates a new redirect page loader from requested URL.
     *
     * @param pageUrl the page url to redirect to
     */
    public RedirectRequest(final String pageUrl) {
        super(pageUrl);
    }

    /**
    * {@inheritDoc}
    */
    @Override
    public void execute(final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {
        response.sendRedirect(getUrl());
    }
}
