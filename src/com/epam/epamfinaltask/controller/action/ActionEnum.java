package com.epam.epamfinaltask.controller.action;

import java.util.HashMap;
import java.util.Map;

public enum ActionEnum {
	    GET_ABOUT_PAGE("/about.html", new LoadHomePageAction(ActionPagePath.ABOUT)),
	    GET_HOME_PAGE("/hello.html", new LoadHomePageAction(ActionPagePath.JSP_PATH)),
	    
	    GET_PROFILE_PAGE("/profile.html", new LoadProfilrPageAction(ActionPagePath.ADMIN_PROFILE)),                     //???????????????????
	   // GET_DRIVER_PROFILE_PAGE("/driver/profile.html", new ("/driver/profile.html")),
	    
	    POST_LOGIN("/authentication.html", new LoginAction("/login.html")),
	    GET_LOGIN_PAGE("/login.html", new LoadSignInPageAction(ActionPagePath.LOGIN_PAGE)),
	
	    POST_REGISTRATION("/createprofile.html", new LoadRedistrationPageAction("/createprofile.html")),
        GET_REGISTRATION_PAGE("/registration.html", new LoadSignInPageAction(ActionPagePath.REGISTRATION_PAGE));
	
	

	private String path;
	private Action action;
	private static final Map<String, ActionEnum> postActionMap;
	private static final Map<String, ActionEnum> getActionMap;
	static {
		getActionMap = new HashMap<>();
		for(ActionEnum url : ActionEnum.values() ) {
			String lol = url.toString();
			if(lol.contains("GET")) {
			getActionMap.put(url.path, url);
			}
		}
		
		postActionMap = new HashMap<>();
		for(ActionEnum url : ActionEnum.values() ) {
			String lol = url.toString();
			if(lol.contains("POST")) {
			postActionMap.put(url.path, url);
			}
		}
	}

	    ActionEnum(String path, Action action) {
            this.setAction(action);
	        this.path = path;

	    }

	  public static ActionEnum getPostUrl(String path) {
		  return postActionMap.get(path);
	  }

	  public static ActionEnum getGetUrl(String path) {
		  return getActionMap.get(path);
	  }
	    

	    public String uri() {

	        return this.path;

	    }

		public Action getAction() {
			return action;
		}

		public void setAction(Action action) {
			this.action = action;
		}
}
