package com.epam.epamfinaltask.controller.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.epam.epamfinaltask.controller.pageloadhelper.ForwardRequest;
import com.epam.epamfinaltask.controller.pageloadhelper.RedirectRequest;
import com.epam.epamfinaltask.controller.pageloadhelper.RequestResult;
import com.epam.epamfinaltask.domain.Driver;
import com.epam.epamfinaltask.domain.Passenger;
import com.epam.epamfinaltask.domain.User;
import com.epam.epamfinaltask.exception.PersistentException;
import com.epam.epamfinaltask.exception.ServiceException;
import com.epam.epamfinaltask.service.DriverService;
import com.epam.epamfinaltask.service.DriverServiceImpl;
import com.epam.epamfinaltask.service.PassengerService;
import com.epam.epamfinaltask.service.PassengerServiceImpl;
import com.epam.epamfinaltask.service.UserService;
import com.epam.epamfinaltask.service.UserServiceImpl;

public class LoginAction extends AbstractAction {
	public LoginAction(String actionPath) {
		super(actionPath);
	}

	private static Logger logger = Logger.getLogger(LoginAction.class);

	/**
	 * {@inheritDoc}
	 * 
	 * @throws PersistentException
	 */
	@Override
	public RequestResult execute(final HttpServletRequest request, final HttpServletResponse response) {
		RequestResult result =  new ForwardRequest(ActionPagePath.ERROR_PAGE);
		String login = request.getParameter("login");
		String password = request.getParameter("password");
		String ph = request.getParameter("phone");
		try {
			if (ph != null && login != null && password != null) {
				int phone = Integer.parseInt(request.getParameter("phone"));
				UserService service = new UserServiceImpl();
				User user = service.searchUserByPhone(phone);

				if (user != null
						&& (user.getRole().name().equals("ADMINISTRATOR") || user.getRole().name().equals("USER"))) {
					PassengerService passengerService = new PassengerServiceImpl();
					Passenger user2 = passengerService.searchUser(login, password);

					if (user2 != null) {
						HttpSession session = request.getSession();
						
						session.setAttribute("authorizedUser", user2);
						session.setAttribute("role", user.getRole().getName());
						session.setAttribute("logged_state", true);
						logger.info(String.format("user \"%s\" is logged in from %s (%s:%s)", login,
								request.getRemoteAddr(), request.getRemoteHost(), request.getRemotePort()));

						result = new RedirectRequest(request.getContextPath() + "/profile.html");

					} else {
						request.setAttribute("message", "��� ������������ ��� ������ �� ���������");
						logger.info(String.format("user \"%s\" unsuccessfully tried to log in from %s (%s:%s)", login,
								request.getRemoteAddr(), request.getRemoteHost(), request.getRemotePort()));
					}
				}
				if (user != null && user.getRole().name().equals("DRIVER")) {
					DriverService driverService = new DriverServiceImpl();
					Driver user3 = driverService.searchDriver(login, password);
					HttpSession session = request.getSession();
					
					session.setAttribute("authorizedUser", user3);
					session.setAttribute("role", user.getRole().getName());
					session.setAttribute("logged_state", true);
					logger.info(String.format("user \"%s\" is logged in from %s (%s:%s)", login,
							request.getRemoteAddr(), request.getRemoteHost(), request.getRemotePort()));

					result = new RedirectRequest(request.getContextPath() + "/driver/profile.html");

				} else {
					request.setAttribute("message", "��� ������������ ��� ������ �� ���������");
					logger.info(String.format("user \"%s\" unsuccessfully tried to log in from %s (%s:%s)", login,
							request.getRemoteAddr(), request.getRemoteHost(), request.getRemotePort()));
				}
			} else {
				request.setAttribute("message", "������� �� �������");
				logger.info(String.format("user \"%s\" unsuccessfully tried to log in from %s (%s:%s)", login,
						request.getRemoteAddr(), request.getRemoteHost(), request.getRemotePort()));
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return result;
	}
}
