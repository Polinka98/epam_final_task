package com.epam.epamfinaltask.controller.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.epamfinaltask.controller.pageloadhelper.RequestResult;

public interface Action {
	   //abstract String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException;
	   /**
	     * Prepares page for loading.
	     *
	     * @param  request  the request
	     * @param  response the response
	     * @return          {@link RequestResult} to load prepared page.
	     */
	    RequestResult execute(HttpServletRequest request,
	            HttpServletResponse response);
}
