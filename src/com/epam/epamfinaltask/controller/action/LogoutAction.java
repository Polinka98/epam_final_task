package com.epam.epamfinaltask.controller.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.epam.epamfinaltask.controller.pageloadhelper.ForwardRequest;
import com.epam.epamfinaltask.controller.pageloadhelper.RequestResult;

public class LogoutAction extends AbstractAction{
	public LogoutAction(String actionPath) {
		super(actionPath);
	}

	private static Logger LOGGER = Logger.getLogger(LogoutAction.class);

	    @Override
	    public RequestResult execute(HttpServletRequest req, HttpServletResponse resp) {
			HttpSession session = req.getSession();
			if (session != null) {
				session.invalidate();
			}

	         return new ForwardRequest(getPath());
	    }

}
