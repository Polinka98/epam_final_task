package com.epam.epamfinaltask.controller.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.epamfinaltask.controller.pageloadhelper.ForwardRequest;
import com.epam.epamfinaltask.controller.pageloadhelper.RequestResult;

/**
 * Created by b.yacenko on 14.06.2017.
 */
public class ErrorAction extends AbstractAction {
    public ErrorAction(String actionPath) {
		super(actionPath);
	}

	@Override
    public RequestResult execute(HttpServletRequest req, HttpServletResponse resp) {
		 return new ForwardRequest(ActionPagePath.ERROR_PAGE);
    }
}
