package com.epam.epamfinaltask.controller.action;

public interface ActionPagePath {
	String JSP_PATH = "/WEB-INF/jsp/hello.jsp";
	String ABOUT = "/WEB-INF/jsp/about.jsp";

	String LOGIN_PAGE = "/WEB-INF/jsp/sign_in.jsp";
	String REGISTRATION_PAGE = "/WEB-INF/jsp/registration.jsp";

	String ERROR_PAGE = "/WEB-INF/jsp/error.jsp";
	
	String ADMIN_PROFILE = "/WEB-INF/jsp/adminProfile.jsp";
	String PASSENGER_PROFILE = "/WEB-INF/jsp/error.jsp";
	String DRIVER_PROFILE= "/WEB-INF/jsp/error.jsp";
}
