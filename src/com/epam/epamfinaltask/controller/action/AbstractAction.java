package com.epam.epamfinaltask.controller.action;
import com.epam.epamfinaltask.domain.User;

/**
 * Created by Polinka
 */
abstract public class AbstractAction implements Action{
	/** Authorized user. **/
	private User authorizedUser;

	  /** The path to next loading resource. */
    private final String path;

    /**
     * Instantiates a new abstract action.
     *
     * @param actionPath the path to next loading resource
     */
    public AbstractAction(final String actionPath) {
        path = actionPath;
    }
    /**
     * Gets authorized user.
     * @return
     */
	public User getAuthorizedUser() {
		return authorizedUser;
	}
/**
 * Sets authorized  user
 * @param authorizedUser
 */
	public void setAuthorizedUser(User authorizedUser) {
		this.authorizedUser = authorizedUser;
	}

    /**
     * Gets the path to next loading resource.
     *
     * @return the path to next loading resource
     */
    public String getPath() {
        return path;
    }
}