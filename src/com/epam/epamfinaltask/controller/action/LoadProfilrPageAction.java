package com.epam.epamfinaltask.controller.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.epamfinaltask.controller.pageloadhelper.ForwardRequest;
import com.epam.epamfinaltask.controller.pageloadhelper.RequestResult;

public class LoadProfilrPageAction extends AbstractAction {

	/**
	 * Instantiates a new load home page action from next loading resource path.
	 *
	 * @param viewPath the path to the next loading resource
	 */
	public LoadProfilrPageAction(String actionPath) {
		super(actionPath);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RequestResult execute(HttpServletRequest request, HttpServletResponse response) {
		return new ForwardRequest(getPath());
	}
}