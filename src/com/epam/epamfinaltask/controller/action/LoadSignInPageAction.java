package com.epam.epamfinaltask.controller.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.epamfinaltask.controller.pageloadhelper.ForwardRequest;
import com.epam.epamfinaltask.controller.pageloadhelper.RequestResult;

public class LoadSignInPageAction extends AbstractAction {

	public LoadSignInPageAction(String actionPath) {
		super(actionPath);
	}

	@Override
	public RequestResult execute(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);
//        if (session != null && (boolean)session.getAttribute("logged_state")) {
//            return new RedirectRequest(request.getContextPath());
//        }
        return new ForwardRequest(getPath());
	}

}
