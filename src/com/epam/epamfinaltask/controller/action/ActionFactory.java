package com.epam.epamfinaltask.controller.action;

import javax.servlet.http.HttpServletRequest;

public class ActionFactory {
    public static Action defineGetCommand(HttpServletRequest req){
    	return ActionEnum.getGetUrl(req.getServletPath()).getAction();
    }

    public static Action definePostCommand(HttpServletRequest req){
    	return ActionEnum.getPostUrl(req.getServletPath()).getAction();
    }
}
