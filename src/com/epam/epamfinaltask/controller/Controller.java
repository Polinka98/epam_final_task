package com.epam.epamfinaltask.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.epam.epamfinaltask.controller.action.Action;
import com.epam.epamfinaltask.controller.action.ActionFactory;
import com.epam.epamfinaltask.controller.pageloadhelper.RequestResult;
import com.epam.epamfinaltask.exception.PersistentException;

@WebServlet(name = "Controller", urlPatterns = "*.html")
@MultipartConfig
public class Controller extends HttpServlet {
	private static Logger logger = Logger.getLogger(Controller.class);

	public void init() {
		try {
			InitServlet.init();
		} catch (PersistentException e) {
			logger.error("It is impossible to initialize application", e);
			destroy();
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		final Action action = ActionFactory
                .defineGetCommand(request);
        if (action!=null) {
            doAction(action, request, response);
        } else {
            send404(response);
        }
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		 final Action action = ActionFactory
	                .definePostCommand(request);
	        if (action!=null) {
	            doAction(action, request, response);
	        } else {
	            send404(response);
	        }
	}

//	private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//		ActionFactory choice = new ActionFactory();
//		Action command = choice.defineCommand(req);
//		String page = command.execute(req, resp);
//		if (page != null) {
//			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
//			dispatcher.forward(req, resp);
//		} else {
//			resp.sendRedirect(ActionPagePath.ERROR_PAGE);
//		}
//	}
	
	 /**
     * Execute given {@link Action}.
     *
     * @param  action           the action to execute
     * @param  request          the request for action
     * @param  response         the response for action
     * @throws IOException      if an input or output error is detected during
     *                          the action execution
     * @throws ServletException if the action could not be handled
     */
    private void doAction(final Action action, final HttpServletRequest request,
            final HttpServletResponse response)
            throws ServletException, IOException {
        final RequestResult loader = action.execute(request, response);
        loader.execute(request, response);
    }
	
    /**
     * Sends 404 error.
     * @param response
     * @throws IOException
     */
	private void send404(final HttpServletResponse response)
            throws IOException {
        response.sendError(HttpServletResponse.SC_NOT_FOUND);
    }
}
