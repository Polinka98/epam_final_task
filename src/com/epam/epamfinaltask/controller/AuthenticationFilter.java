package com.epam.epamfinaltask.controller;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

public class AuthenticationFilter implements Filter {
	private static Logger logger = Logger.getLogger(AuthenticationFilter.class);

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {}

	@Override
	public void destroy() {}

	@Override

		public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
				throws ServletException, IOException {

			HttpServletRequest request = (HttpServletRequest) req;

			HttpServletResponse response = (HttpServletResponse) resp;

			HttpSession session = request.getSession(false);

			String role = "";

			boolean loggedIn = (session != null && session.getAttribute("logged_state") != null
					&& (Boolean) session.getAttribute("logged_state"));

			if (session != null && session.getAttribute("role") != null) {

				role = (String) session.getAttribute("role");

			}

			if (loggedIn) {

				if (role.equals("Administrator") && request.getRequestURI().contains("admin")) {

					chain.doFilter(request, response);

				} else if (role.equals("User") && request.getRequestURI().contains("admin")) {
					response.sendRedirect(request.getContextPath() + "/login.html");
				}  else if (role.equals("User") && request.getRequestURI().contains("driver")) {
					response.sendRedirect(request.getContextPath() + "/login.html");
				}  else if (role.equals("Driver") && request.getRequestURI().contains("admin")) {
						response.sendRedirect(request.getContextPath() + "/login.html");
				}  else if (role.equals("Driver") && request.getRequestURI().contains("user")) {
					response.sendRedirect(request.getContextPath() + "/login.html");
				}  else if (role.equals("Administrator") && request.getRequestURI().contains("driver")) {
						response.sendRedirect(request.getContextPath() + "/login.html");
				} else {
					chain.doFilter(request, response);
				}
			}
//			 else {
//
//				response.sendRedirect(request.getContextPath() + "/hello.html");
//
//			}

		}
}
