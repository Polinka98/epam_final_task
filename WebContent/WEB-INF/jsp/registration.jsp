<!DOCTYPE html>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="u"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>


<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<fmt:setLocale value="${sessionScope.locale}"/>

<fmt:bundle basename="message.local">

    <fmt:message key="loginpage.title" var="title"/>

    <fmt:message key="loginpage.form.header" var="formHeader"/>
    
    <fmt:message key="loginpage.form.header2" var="formHeader2"/>

    <fmt:message key="loginpage.form.loginField" var="loginField"/>
    
    <fmt:message key="loginpage.form.phoneField" var="phoneField"/>

    <fmt:message key="loginpage.hint.login" var="loginHint"/>

    <fmt:message key="loginpage.form.passField" var="passwordField"/>

    <fmt:message key="btn.form.login" var="loginButton"/>
    
    <fmt:message key="btn.form.remember" var="remember"/>

</fmt:bundle>
		<title>Buber</title>
	
	<c:url value="${pageContext.request.contextPath}/main.css" var="cssUrl"/>
	<link rel="stylesheet" type="text/css" href="${cssUrl}">
	
	<link rel="stylesheet"
	href="${pageContext.request.contextPath}/main.css" var="cssUrl">
	
	<c:url value="/js" var="javascriptUrl"/>
	<SCRIPT type="text/javascript" src="${javascriptUrl}/main.js"></SCRIPT>
	<c:if test="${not empty message}">
		<SCRIPT type="text/javascript">
			startMessage = "${message}";
	</SCRIPT> 
	</c:if>
	<c:if test="${not empty validator}">
		<SCRIPT type="text/javascript" src="${javascriptUrl}/validator.js"></SCRIPT>
		<SCRIPT type="text/javascript" src="${javascriptUrl}/${validator}"></SCRIPT>
	</c:if>
	
<link href="${pageContext.request.contextPath}/style/bootstrap.min.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath}/style/bootstrap-theme.min.css"
	rel="stylesheet" />
	
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="http://bootstraptema.ru/plugins/font-awesome/4-4-0/font-awesome.min.css" />
<script src="http://bootstraptema.ru/plugins/jquery/jquery-1.11.3.min.js"></script>
<script src="http://bootstraptema.ru/plugins/2015/b-v3-3-6/bootstrap.min.js"></script>

<style>

h1, h2, label{
	color: white;
	font-family: courier;
}

p {
	color: white;
	font-family: courier;
}
* {
    box-sizing: border-box;
}
ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
    position: fixed;
    top: 0;
    width: 100%;
}

li {
    float: right;
}

li a {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}
body {
  background-image: url(${pageContext.request.contextPath}/img/asphalt.jpg);
  background-position: center center;
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-size: cover;
}
 </style>
 </head>
	<body>
	<c:url value="/registration.html" var="registrationUrl"/>

 <ul>
  <li><a class="active" data-toggle="tab" href="hello.html">Home</a></li>
</ul>
        <div id="main">
            <div id="inputField" align="center">
				<br><br><h2>Please, sign up</h2>
                <form class="form-signin" action="${registrationUrl}" method="post">
                <input type="hidden" name="action" value="registration">
                       
                        <input type="text" class="form-control btn-block" name = "name" id="name" placeholder="Name" required autofocus><br>
                      
                        <input type="text" class="form-control input-lg" name = "surname" id="surname" placeholder="Surname" required> <br>
                    <input type="email"  class="form-control" name = "email" id="email" placeholder="Email" required> <br> 
					<label for="telephone">+375</label>
                   <input type="number" class="form-control " name = "phone" id="phone" placeholder="Telephone" required><br>
				   <input type="password" name = "password" id="password" class="form-control " placeholder="Password" required>

                <button type="submit" class="btn btn-default btn-primary">
                    Sign up
                </button>
                </form>
                
            </div>
        </div>	
</body>
</html>
