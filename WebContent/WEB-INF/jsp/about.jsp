<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="u"%>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" />
<script src="http://bootstraptema.ru/plugins/jquery/jquery-2.1.4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<br><br><br>

<style>
body {
	background-image:
		url(${pageContext.request.contextPath}/img/asphalt.jpg);
	background-position: center center;
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-size: cover;
}
 h1, h2, label { 
 	color: white;
	font-family: courier; 
 } 
#main {
 background-color: #f2f2f2;
 padding: 20px;
-webkit-border-radius: 4px;
 -moz-border-radius: 4px;
 -ms-border-radius: 4px;
 -o-border-radius: 4px;
 border-radius: 4px;
 border-bottom: 4px solid #ddd;
}
#accordion .panel{
 border: none;
 box-shadow: none;
 border-radius: 0;
 margin-bottom: -5px;
}
#accordion .panel-heading{
 padding: 0;
 border-radius: 0;
 border: none;
 text-align: center;
}
#accordion .panel-title a{
 display: block;
 padding: 25px 30px;
 font-size: 16px;
 font-weight: bold;
 color: #fff;
 background: #333;
 border-bottom: 1px solid #4a4a4a;
 position: relative;
 transition: all 0.5s ease 0s;
}
#accordion .panel-title a:hover{
 background: #4a4a4a;
}
#accordion .panel-title a:after,
#accordion .panel-title a.collapsed:after{
 content: "\f067";
 font-family: FontAwesome;
 font-size: 15px;
 font-weight: 200;
 position: absolute;
 top: 25px;
 left: 15px;
 transform: rotate(135deg);
 transition: all 0.5s ease 0s;
}
#accordion .panel-title a.collapsed:after{
 transform: rotate(0deg);
}
#accordion .panel-body{
 background: #167ea0;
 padding: 0 0 0 40px;
 border: none;
 position: relative;
}
#accordion .panel-body p{
 font-size: 14px;
 color: #fff;
 line-height: 25px;
 background: #3296b7;
 padding: 30px;
 margin: 0;
}
#accordion .panel-collapse .panel-body p{
 opacity: 0;
 transform: scale(0.9);
 transition: all 0.5s ease-in-out 0s;
}
#accordion .panel-collapse.in .panel-body p{
 opacity: 1;
 transform: scale(1);
}
ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
    position: fixed;
    top: 0;
    width: 100%;
}

li {
    float: right;
}

li a {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}
</style>

<ul>
  <li><a class="active" data-toggle="tab" href="/hello.html">Home</a></li>
</ul>
<div class="container">
<div class="row">

 <div class="col-md-6 col-md-offset-3">
 <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

 <div class="panel panel-default">
 <div class="panel-heading" role="tab" id="headingOne">
 <h4 class="panel-title">
 <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
 SECTION 1
 </a>
 </h4>
 </div>
 <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
 <div class="panel-body">
 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lacinia lacinia mauris, at ultricies leo ornare nec. Vivamus eu est vel felis dignissim tempor. Nulla facilisi. Duis molestie tortor ac tempor volutpat. Phasellus nec mi aliquet, sollicitudin neque eget, consectetur libero. Vestibulum sollicitudin sapien libero, egestas tempus eros scelerisque eu.</p>
 </div>
 </div>
 </div>

 <div class="panel panel-default">
 <div class="panel-heading" role="tab" id="headingTwo">
 <h4 class="panel-title">
 <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
 SECTION 2
 </a>
 </h4>
 </div>
 <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
 <div class="panel-body">
 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lacinia lacinia mauris, at ultricies leo ornare nec. Vivamus eu est vel felis dignissim tempor. Nulla facilisi. Duis molestie tortor ac tempor volutpat. Phasellus nec mi aliquet, sollicitudin neque eget, consectetur libero. Vestibulum sollicitudin sapien libero, egestas tempus eros scelerisque eu.</p>
 </div>
 </div>
 </div>

 <div class="panel panel-default">
 <div class="panel-heading" role="tab" id="headingThree">
 <h4 class="panel-title">
 <a class="" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
 SECTION 3
 </a>
 </h4>
 </div>
 <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
 <div class="panel-body">
 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lacinia lacinia mauris, at ultricies leo ornare nec. Vivamus eu est vel felis dignissim tempor. Nulla facilisi. Duis molestie tortor ac tempor volutpat. Phasellus nec mi aliquet, sollicitudin neque eget, consectetur libero. Vestibulum sollicitudin sapien libero, egestas tempus eros scelerisque eu.</p>
 </div>
 </div>
 </div>

 <div class="panel panel-default">
 <div class="panel-heading" role="tab" id="headingFour">
 <h4 class="panel-title">
 <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
 SECTION 4
 </a>
 </h4>
 </div>
 <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
 <div class="panel-body">
 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lacinia lacinia mauris, at ultricies leo ornare nec. Vivamus eu est vel felis dignissim tempor. Nulla facilisi. Duis molestie tortor ac tempor volutpat. Phasellus nec mi aliquet, sollicitudin neque eget, consectetur libero. Vestibulum sollicitudin sapien libero, egestas tempus eros scelerisque eu.</p>
 </div>
 </div>
 </div>

 </div>
 </div>

</div><!-- ./row -->
</div><!-- ./container -->