<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

	<c:url value="${pageContext.request.contextPath}/main.css" var="cssUrl"/>
	<link rel="stylesheet" type="text/css" href="${cssUrl}">
	<c:url value="/js" var="javascriptUrl"/>
	<SCRIPT type="text/javascript" src="${javascriptUrl}/main.js"></SCRIPT>
	<c:if test="${not empty message}">
		<SCRIPT type="text/javascript">
			startMessage = "${message}";
		</SCRIPT>
	</c:if>
	<c:if test="${not empty validator}">
		<SCRIPT type="text/javascript" src="${javascriptUrl}/validator.js"></SCRIPT>
		<SCRIPT type="text/javascript" src="${javascriptUrl}/${validator}"></SCRIPT>
	</c:if>
	
<link href="${pageContext.request.contextPath}/style/bootstrap.min.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath}/style/bootstrap-theme.min.css"
	rel="stylesheet" />
	
	
<link rel="stylesheet" href="http://bootstraptema.ru/plugins/font-awesome/4-4-0/font-awesome.min.css" />
<script src="http://bootstraptema.ru/plugins/jquery/jquery-1.11.3.min.js"></script>
<script src="http://bootstraptema.ru/plugins/2015/b-v3-3-6/bootstrap.min.js"></script>

<style>
body {
	background-image:
		url(${pageContext.request.contextPath}/img/asphalt.jpg);
	background-position: center center;
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-size: cover;
}
h1, h2, label {
	color: white;
	font-family: courier;
}
</style>
</head>
