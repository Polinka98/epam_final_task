<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="message.local">
    <fmt:message key="errorpage.common.title" var="title"/>
    <fmt:message key="errorpage.common.header" var="errorHeader"/>
    <fmt:message key="errorpage.${requestScope.error}" var="errorMessage"/>
    <fmt:message key="btn.back" var="backButton"/>
</fmt:bundle>

<title>${title}</title>

<jsp:include page="/WEB-INF/jsp/template/style.jsp"/>
	<body>
<div class="container">
    <h1 class="mt-4 mb-3">${errorHeader}</h1>
    <div class="jumbotron">
        <h3 class="display-4">${errorMessage}</h3>
    </div>
    <input type="button" value="${backButton}" onCLick="history.back()">
</div>
</body>
</html>