<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="u"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<fmt:setLocale value="${sessionScope.locale}" />

<fmt:bundle basename="message.local">

	<fmt:message key="loginpage.title" var="title" />

	<fmt:message key="loginpage.form.header" var="formHeader" />

	<fmt:message key="loginpage.form.header2" var="formHeader2" />

	<fmt:message key="loginpage.form.loginField" var="loginField" />

	<fmt:message key="loginpage.form.phoneField" var="phoneField" />

	<fmt:message key="loginpage.hint.login" var="loginHint" />

	<fmt:message key="loginpage.form.passField" var="passwordField" />

	<fmt:message key="btn.form.login" var="loginButton" />

	<fmt:message key="btn.form.remember" var="remember" />
</fmt:bundle>

<title>Start</title>
<script type="text/javascript" src="${javascriptUrl}/main.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/main.css" var="cssUrl">
	<link href="${pageContext.request.contextPath}/style/bootstrap.min.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath}/style/bootstrap-theme.min.css"
	rel="stylesheet" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="http://bootstraptema.ru/plugins/font-awesome/4-4-0/font-awesome.min.css" />
<script src="http://bootstraptema.ru/plugins/jquery/jquery-1.11.3.min.js"></script>
<script src="http://bootstraptema.ru/plugins/2015/b-v3-3-6/bootstrap.min.js"></script>

<style>
body {
	background-image:
		url(${pageContext.request.contextPath}/img/asphalt.jpg);
	background-position: center center;
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-size: cover;
}

h1, h2, label {
	color: white;
	font-family: courier;
}
</style>
</head>
<%-- <jsp:include page="/WEB-INF/jsp/template/style.jsp"/> --%>
<body>
	<c:url value="/hello.html" var="loginUrl" />
	<ul>
		<li><A href="/buber/login.html" class="active" >Log	in</A></li>
		<li><a href="/buber/registration.html">Sign up</a></li>
		<li><a href="/buber/about.html">About</a></li>
		<li class="dropdown"><a href="javascript:void(0)" class="dropbtn">Language</a>
			<div class="dropdown-content">
				<a href="#">EN</a> <a href="#">RU</a> <a href="#">BE</a>
			</div></li>
	</ul>

	<div class="tab-pane fade" id="about"></div>

	<div id="main">

		<div id="inputField" align="center">
			<br>
			<br>
			<br> <img
				src="${pageContext.request.contextPath}/img/buber_logo.png"
				alt="buber" width="160" height="155"><br>
			<h1>Welcome to Buber!</h1>
			<br>
			<h2>Have a good trip!</h2>
			<br> <br>

			<p class="mt-5 mb-3 text-muted">&copy; 2018 -</p>
		</div>
	</div>

</body>
</html>