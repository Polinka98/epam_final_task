<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="u"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> pleaseeeee </title>

<c:url value="/js" var="javascriptUrl"/>
        <script type="text/javascript" src="${javascriptUrl}/main.js"></script>
        <script type="text/javascript" src="${javascriptUrl}/validator-of-change-password-form.js"></script>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/style/maain.css" var="cssUrl">
        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" />
		
		<link rel="stylesheet" href="http://bootstraptema.ru/plugins/2015/bootstrap3/bootstrap.min.css" />
<link rel="stylesheet" href="http://bootstraptema.ru/plugins/font-awesome/4-4-0/font-awesome.min.css" />
<script src="http://bootstraptema.ru/plugins/jquery/jquery-1.11.3.min.js"></script>
<script src="http://bootstraptema.ru/plugins/2015/b-v3-3-6/bootstrap.min.js"></script>


<style>
body {
	background-image:
		url(${pageContext.request.contextPath}/img/asphalt.jpg);
	background-position: center center;
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-size: cover;
}
 h1, h2, label { 
 	color: white;
	font-family: courier; 
 } 
#main {
 background-color: #f2f2f2;
 padding: 20px;
-webkit-border-radius: 4px;
 -moz-border-radius: 4px;
 -ms-border-radius: 4px;
 -o-border-radius: 4px;
 border-radius: 4px;
 border-bottom: 4px solid #ddd;
}
#real-estates-detail #author img {
 -webkit-border-radius: 100%;
 -moz-border-radius: 100%;
 -ms-border-radius: 100%;
 -o-border-radius: 100%;
 border-radius: 100%;
 border: 5px solid #ecf0f1;
 margin-bottom: 10px;
}
#real-estates-detail .sosmed-author i.fa {
 width: 30px;
 height: 30px;
 border: 2px solid #bdc3c7;
 color: #bdc3c7;
 padding-top: 6px;
 margin-top: 10px;
}
.panel-default .panel-heading {
 background-color: #f2f2f2;
}
#real-estates-detail .slides li img {
 height: 450px;
}
ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
    position: fixed;
    top: 0;
    width: 100%;
}

li {
    float: right;
}

li a {
    display: inline-block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}
</style>
</head>
<body>

	<c:if test="${not empty authorizedUser}">
	<br><br>
 
 <div class="col-lg-4 col-md-4 col-xs-12">
 <div class="panel panel-default">
 <div class="panel-heading">
 <header class="panel-title">
 <div class="text-center">
 <br><strong>Site user</strong>
 </div>
 </header>
 </div>
<div class="panel-body">
 <div class="text-center" id="author">
 <img src="${pageContext.request.contextPath}/img/for_user_profile.jpg" alt="buber" width="195" height="215">
 <h3>${authorizedUser.user.name} ${authorizedUser.user.surname}  ффф</h3>
 <p>Just a nice person.</p><br>
 
 </div>
 </div>
 </div>
 </div>

<div class="col-lg-8 col-md-8 col-xs-12">
 <div class="panel">
 <div class="panel-body">
 <ul id="myTab" class="nav nav-pills">
 <li class=""><a href="/buber/profile.html" >about me</a></li>
 <li class="active"><a href="#contact" >Edit Profile</a></li>
 <li class=""><a href="/buber/request.html" >Request a ride</a></li>
 
 <c:if test="${sessionScope.role == 'ADMINISTRATOR'}">
 <li class=""><a href="buber/adminfunctions.html">Admin functions</a></li>
 </c:if>
 
 <li class=""><a href="/buber/hello.html" >Logout</a></li>
 </ul>
 
 <div id="myTabContent" class="tab-content">
<hr>


<div class="tab-pane fade" id="contact">
 <p></p>
 <c:url value="/profile/save.html" var="editUrl"/>  
 <form role="form"  action="${editUrl}" method="post" onsubmit="return validateChangePassword(this)">
 <input type="hidden" name="action" value="edit_profile">
 <div class="form-group">
 <label>New e-mail</label>
 <input type="email" class="form-control rounded" placeholder="e-mail" required autofocus>
 </div>
 <div class="form-group">
 <label>New password</label>
 <input type="password" class="form-control rounded" name="new-password" id="new-password-1" placeholder="Polink983564a" required autofocus>
 </div>
 <div class="form-group">
 <label>Repeat password</label>
 <input type="password" class="form-control rounded" id="new-password-2" placeholder="password" required autofocus>
 </div>
 <div class="form-group">
 <button type="submit" class="btn btn-default btn-primary" data-original-title="" title="">Save</button>
 </div>
 </form>
 </div>
 
 </div>
 </div>
</div><!-- /.main -->
</div><!-- /.container -->
	
</c:if>	

</body>
</html>




