<!DOCTYPE html>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="u"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>


<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<fmt:setLocale value="${sessionScope.locale}"/>

<fmt:bundle basename="message.local">

    <fmt:message key="loginpage.title" var="title"/>

    <fmt:message key="loginpage.form.header" var="formHeader"/>
    
    <fmt:message key="loginpage.form.header2" var="formHeader2"/>

    <fmt:message key="loginpage.form.loginField" var="loginField"/>
    
    <fmt:message key="loginpage.form.phoneField" var="phoneField"/>

    <fmt:message key="loginpage.hint.login" var="loginHint"/>

    <fmt:message key="loginpage.form.passField" var="passwordField"/>

    <fmt:message key="btn.form.login" var="loginButton"/>
    
    <fmt:message key="btn.form.remember" var="remember"/>

</fmt:bundle>
		<title>login</title>
	
	<link rel="stylesheet"
	href="${pageContext.request.contextPath}/main.css" var="cssUrl">
	<c:url value="/js" var="javascriptUrl"/>
	<SCRIPT type="text/javascript" src="${javascriptUrl}/main.js"></SCRIPT>
	<c:if test="${not empty message}">
		<SCRIPT type="text/javascript">
// 			startMessage = "${message}";
<!-- 		</SCRIPT> -->
	</c:if>
	<c:if test="${not empty validator}">
		<SCRIPT type="text/javascript" src="${javascriptUrl}/validator.js"></SCRIPT>
		<SCRIPT type="text/javascript" src="${javascriptUrl}/${validator}"></SCRIPT>
	</c:if>
	
<link href="${pageContext.request.contextPath}/style/bootstrap.min.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath}/style/bootstrap-theme.min.css"
	rel="stylesheet" />
	
	
<link rel="stylesheet" href="http://bootstraptema.ru/plugins/font-awesome/4-4-0/font-awesome.min.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<style>
 body {
	background-image:
		url(${pageContext.request.contextPath}/img/asphalt.jpg);
	background-position: center center;
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-size: cover;
}
 h1, h2, label { 
 	color: white;
	font-family: courier; 
 } 
</style>
</head>

<%-- <jsp:include page="/WEB-INF/jsp/template/style.jsp"/> --%>
	<body>
	<c:url value="/authentication.html" var="loginUrl"/>
	 <ul>
  <li><a class="active" href="/buber/hello.html">Home</a></li>
</ul>
            <div id="inputField" align="center">
                <br><br><h1> Please, sign in</h1><br>
                <form class="form-signin" action="${loginUrl}" method="post"> 
                <input type="hidden" name="action" value="login">

				<label for="telephone">+375</label>
                   <input type="number" class="form-control " name = "phone" id="telephone" placeholder="Telephone"  required autofocus><br>
				   <input type="email" class="form-control " id="email" name = "login" placeholder="Email" required ><br>
				   <input type="password" id="password" name = "password" class="form-control " placeholder="Password" required >
				   <div class="checkbox mb-3">
                      <label>
                      <input type="checkbox" value="remember-me"> Remember me <!-- ${remember} -->
                      </label>
                    </div>

                <button type="submit" class="btn btn-default btn-primary">
                    Sign in<!-- ${loginButton} -->
                </button>
                </form>
            </div>
	
</body>
</html>
