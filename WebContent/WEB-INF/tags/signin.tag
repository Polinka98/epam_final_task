<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@tag language="java" pageEncoding="UTF-8"%>
<%@attribute name="title" required="true" rtexprvalue="true" type="java.lang.String"%>
<%@attribute name="message" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@attribute name="validator" required="false" rtexprvalue="true" type="java.lang.String"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="u"%>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Buber - ${title}</title>
	<c:url value="${pageContext.request.contextPath}/main.css" var="cssUrl"/>
	<link rel="stylesheet" type="text/css" href="${cssUrl}">
	<c:url value="/js" var="javascriptUrl"/>
	<SCRIPT type="text/javascript" src="${javascriptUrl}/main.js"></SCRIPT>
	<c:if test="${not empty message}">
		<SCRIPT type="text/javascript">
			startMessage = "${message}";
		</SCRIPT>
	</c:if>
	<c:if test="${not empty validator}">
		<SCRIPT type="text/javascript" src="${javascriptUrl}/validator.js"></SCRIPT>
		<SCRIPT type="text/javascript" src="${javascriptUrl}/${validator}"></SCRIPT>
	</c:if>
	
<link href="${pageContext.request.contextPath}/style/bootstrap.min.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath}/style/bootstrap-theme.min.css"
	rel="stylesheet" />
	
	
<link rel="stylesheet" href="http://bootstraptema.ru/plugins/font-awesome/4-4-0/font-awesome.min.css" />
<script src="http://bootstraptema.ru/plugins/jquery/jquery-1.11.3.min.js"></script>
<script src="http://bootstraptema.ru/plugins/2015/b-v3-3-6/bootstrap.min.js"></script>

<style>
body{background:url(asphalt.jpg)}
h1, h2, label {
	color: white;
	font-family: courier;
}
</style>
</head>
<body>
<u:menu/>
<div id="page">
<jsp:doBody/>
</div>
</body>
</html>