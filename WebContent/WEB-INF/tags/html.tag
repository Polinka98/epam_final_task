<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@tag language="java" pageEncoding="UTF-8"%>
<%@attribute name="title" required="true" rtexprvalue="true"
	type="java.lang.String"%>
<%@attribute name="message" required="false" rtexprvalue="true"
	type="java.lang.String"%>
<%@attribute name="validator" required="false" rtexprvalue="true"
	type="java.lang.String"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="u"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Start_Page</title>
<script type="text/javascript" src="${javascriptUrl}/main.js"></script>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/main.css" var="cssUrl">

<link href="${pageContext.request.contextPath}/style/bootstrap.min.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath}/style/bootstrap-theme.min.css"
	rel="stylesheet" />

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" />
<script src="http://bootstraptema.ru/plugins/jquery/jquery-2.1.4.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style>
body {
	background-image:
		url(${pageContext.request.contextPath}/img/asphalt.jpg);
	background-position: center center;
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-size: cover;
}

h1, h2, label {
	color: white;
	font-family: courier;
}
</style>
</head>


<body>
	<ul>
		<li><A href="/WEB-INF/jsp/registration.jsp" data-toggle="tab">Sign in</A></li> <!-- GO TO JSP REGESTRATION -->
		
		<!-- GO TO JSP SIGN UP  -->
		<li><a href="${pageContext.request.contextPath}/WEB-INF/jsp/sign_up_as_user.html" data-toggle="tab">Sign up</a></li>
			
		<li><a href="#about" data-toggle="tab">About</a></li>
		<li class="dropdown"><a href="javascript:void(0)" class="dropbtn">Language</a>
			<div class="dropdown-content">
				<a href="#">EN</a> <a href="#">RU</a> <a href="#">BE</a>
			</div></li>
	</ul>

	<div class="tab-pane fade" id="about"></div>

	<div id="main">

		<div id="inputField" align="center">
			<br>
			<img src="${pageContext.request.contextPath}/img/buber_logo.png"
				alt="buber" width="160" height="155"><br>
			<h1>Welcome to Buber!</h1>
			<br>
			<h2>Have a good trip!</h2>
			<br>
			<br>

			<p class="mt-5 mb-3 text-muted">&copy; 2018 -</p>
		</div>
	</div>

</body>
</html>