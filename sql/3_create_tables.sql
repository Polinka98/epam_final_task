USE `buber`;

CREATE TABLE `buber`.`user` (
  `phone_number` INT(10) NOT NULL DEFAULT 0,
  `name` VARCHAR(15) NOT NULL ,
   /*
	 * 0 - admin (Role.ADMINISTRATOR)
	 * 1 - driver (Role.DRIVER)
	 * 2 - user (Role.USER)
	 */
  `role` TINYINT(2) NOT NULL,
  `surname` VARCHAR(15) NOT NULL,
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  INDEX `phone` (`phone_number` ASC),
  PRIMARY KEY (`id`),
  UNIQUE INDEX `phone_number_UNIQUE` (`phone_number` ASC))
ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;


CREATE TABLE `buber`.`driver` (
  `id` INT UNSIGNED NOT NULL,
  `isBan` TINYINT(1) NOT NULL,
  `reputation` INT NOT NULL,
  `isActive` TINYINT(1) NOT NULL,
  `isFree` TINYINT(1) NOT NULL,
  `login` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `login_UNIQUE` (`login` ASC),
  INDEX `password` (`password` ASC),
  CONSTRAINT `id`
    FOREIGN KEY (`id`)
    REFERENCES `buber`.`user` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;


CREATE TABLE `buber`.`taxi` (
  `auto_number` VARCHAR(15) NOT NULL,
  `image` VARCHAR(255) NULL DEFAULT NULL ,
  `is_free` TINYINT(1) NOT NULL DEFAULT 1,
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`))
ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;


CREATE TABLE `buber`.`driver_taxi` (
  `id` SMALLINT(6) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `is_free` TINYINT(1) NOT NULL ,
  `driver_id` INT UNSIGNED NOT NULL,
  `taxi_id` INT(10) UNSIGNED NOT NULL,
  `current_coord_longitude` DOUBLE NOT NULL,
  `current_coord_latitude` DOUBLE NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `driver_id_UNIQUE` (`driver_id` ASC),
  UNIQUE INDEX `taxi_id_UNIQUE` (`taxi_id` ASC),
  CONSTRAINT `driver_id`
    FOREIGN KEY (`driver_id`)
    REFERENCES `buber`.`driver` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `taxi`
    FOREIGN KEY (`taxi_id`)
    REFERENCES `buber`.`taxi` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;


CREATE TABLE `buber`.`passenger` (
  `id` INT UNSIGNED NOT NULL,
  `cash_account` DECIMAL NOT NULL,
  `status` TINYINT(2) NOT NULL,
  `isBan` TINYINT(1) NOT NULL,
  `reputation` INT NOT NULL,
  `login` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `login_UNIQUE` (`login` ASC),
  INDEX `password` (`password` ASC),
  CONSTRAINT `id_user`
    FOREIGN KEY (`id`)
    REFERENCES `buber`.`user` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;



CREATE TABLE `buber`.`request` (
  `id` SMALLINT(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_current_coord_latitude` DOUBLE NOT NULL,
  `final_coordinate_latitude` DOUBLE NOT NULL,
  `data_time_of_request` DATETIME NOT NULL ,
  `data_time_of_shipping` TIME NOT NULL,
  `data_time_of_arrival` TIME NOT NULL,
  `makk_for_driver` TINYINT(4) NOT NULL DEFAULT '0' ,
  `mark_for_user` TINYINT(4) NOT NULL DEFAULT '0' ,
  `archive` TINYINT(1) NOT NULL DEFAULT '0',
  `user_current_coordinate_longitude` DOUBLE NOT NULL,
  `user_final_coordinate_longitude` DOUBLE NOT NULL,
  `user_id` INT UNSIGNED NOT NULL,
  `driver_taxi_id` SMALLINT(6) UNSIGNED NOT NULL,
  `payment` TINYINT(1) NOT NULL,
  `price` DECIMAL NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC),
  UNIQUE INDEX `driver_taxi_id_UNIQUE` (`driver_taxi_id` ASC),
  CONSTRAINT `user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `buber`.`passenger` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `driver_taxi_id`
    FOREIGN KEY (`driver_taxi_id`)
    REFERENCES `buber`.`driver_taxi` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;
